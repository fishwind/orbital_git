﻿using UnityEngine;
using System.Collections;

public class tracker : MonoBehaviour {

	public bool sawPlayer = false;

	RaycastHit hit;

	void FixedUpdate() {
		if(Physics.Raycast(transform.position, transform.forward, out hit)) {
			GameObject hitobj = hit.collider.gameObject;
			//print (Vector3.Distance (this.transform.position, hitobj.transform.position));
			if (hitobj.tag == "Player") {
				sawPlayer = true;
			} else {
				sawPlayer = false;
			}
		} 
	}


}
