﻿using UnityEngine;
using System.Collections;

public class Acid_prefab : MonoBehaviour {

	Vector3 targetScale;
	bool hasHit = false;
	float scaleSpeed = 15f;
	float scaleDuration = 9f;
	float lifeDuration = 13.5f;
	float lifeTimer = 0f;
	float scaleTimer = 0f;
	float hurtTimer = 0f;
	float hurtDelay = 0.3f;

	void Start() {
		targetScale = transform.localScale * 120f;
		targetScale.z = targetScale.z / 120f;
		lifeTimer = Time.time + lifeDuration;
		scaleTimer = Time.time + scaleDuration;
	}
	void Update() {
		if (Time.time < scaleTimer) {
			transform.localScale = Vector3.MoveTowards (transform.localScale, targetScale, scaleSpeed * Time.deltaTime);
		}
		if (Time.time > lifeTimer) {
			GetComponent<MeshCollider> ().enabled = false;
		}
		if (Time.time > hurtTimer) {
			hurtTimer = Time.time + hurtDelay;
			hasHit = false;
		}
	}
	void OnTriggerStay(Collider hit) {
		if (hasHit == false && hit.gameObject.tag == "Player") {
			hasHit = true;
			hit.gameObject.GetComponent<PlayerController> ().player_hurt_small ();
		}
	}
}
