﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class PA_suicide : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageSphere dmgSphere;
	public GameObject body;
	public ParticleSystem particle_explode;
	public float chase_distance = 120f;
	public float dmgSphere_amount = 200f;
	public float dmgSphere_force = 2500;
	public float dmgSphere_forceUp = 3f;
	public bool dmgSphere_isBig = true;

	Animator anim;
	AudioController audioController;
	GameObject player;
	NavMeshAgent nav;
	float trigger_distance = 4f;
	bool canChase = false;
	bool hasTriggered = false;
	bool isDead = false;

	void Awake() {
		health.enemyType = 1;
	}
	void Start() {
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		nav = GetComponent<NavMeshAgent> ();
	} 
	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			} else {
				float distance = Vector3.Distance (this.transform.position, player.transform.position);
				if (distance <= chase_distance) {
					chase ();
				} else {
					audioController.stopAudio ();
				}
				if (!hasTriggered && distance <= trigger_distance) {
					hasTriggered = true;
					triggerCounter ();
				}
			}
		}
	}

	void chase() {
		if (canChase) {
			nav.SetDestination (player.transform.position);
			anim.SetTrigger ("A_run");
			audioController.playLoop (1);
		}
	}

	void triggerCounter() {
		body.GetComponent<Animator> ().SetTrigger ("A_trigger");
	}

	void OnCollisionEnter(Collision hit) {
		if (hit.gameObject.tag == "ground") {
			GetComponent<Rigidbody> ().isKinematic = true;
			GetComponent<NavMeshAgent> ().enabled = true;
			if (!canChase) {
				canChase = true;
			}
		}
	}

	public void die() {
		GameManager.stats_enemiesKilled += 1;
		isDead = true;
		audioController.stopAudio ();
		particle_explode.Stop ();
		particle_explode.Play ();
		dmgSphere.startDmgSphere (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
		body.SetActive (false);
		audioController.playOnce (0);
		GetComponent<SphereCollider> ().enabled = false;
		StartCoroutine (clearbody ());
	}
	IEnumerator clearbody() {
		nav.enabled = false;
		yield return new WaitForSeconds(1f);
		dmgSphere.endDmgSphere ();
		Destroy (this.gameObject, 4f);
	}
}
