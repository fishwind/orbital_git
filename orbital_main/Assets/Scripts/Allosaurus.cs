﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animation))]
public class Allosaurus : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageSphere dmgSphere;
	public GameObject spawnpoint;
	public NavMeshAgent nav;
	public float close_enough_distance = 40;
	public float dmgSphere_amount = 200f;
	public float dmgSphere_force = 1500;
	public float dmgSphere_forceUp = 2f;
	public bool dmgSphere_isBig = true;	// big sphere trigger player's hurtBig animation

	Animation anim;
	string currentState;
	GameObject player;
	AudioController audioController;

	void Awake() {
		health.enemyType = 3;
	}

	void Start() {
		anim = GetComponent<Animation> ();
		audioController = GetComponent<AudioController> ();
		currentState = "idle";
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update() {
		if (health.currentHealth <= 0) {
			currentState = "die";
		}
		switch (currentState) {
			case "idle":
				normal_state ();
				break;
			case "attack":
				attack_state ();
				break;
			case "die":
				die_state ();
				break;
		}
	}
	void normal_state() {	// reactive or idle?
		float distanceDifferent = Vector3.Distance (player.transform.position, this.transform.position);
		Vector3 vectorDifferent = player.transform.position - this.transform.position;
		float angleDifferent = Vector3.Angle (vectorDifferent, transform.forward);
		if (distanceDifferent > close_enough_distance ){// || angleDifferent > 45) {
			anim.CrossFade ("Allosaurus_IdleAggressive");
		} else {
			if (Vector3.Distance (spawnpoint.transform.position, player.transform.position) <= 3.2f) {
				currentState = "attack";
			} else {
				anim.CrossFade ("Allosaurus_Run");
				nav.SetDestination (player.transform.position);
			}
		}
	}

	void attack_state() {						// at the end of att, instantiate damage sphere
		anim.CrossFade ("Allosaurus_Attack02");	// at the end of att, go back normal_state if not in position
	}

	void die_state() {
		anim.CrossFade ("Allosaurus_Die");
		nav.enabled = false;
	}
	void clearBody() {
		foreach (Collider collider in health.GetComponents<Collider>()) {
			collider.enabled = false;
		}
		GameManager.stats_enemiesKilled += 1;
		Destroy (nav.gameObject, 5f);
	}
	void spawn_damage_sphere() {
		dmgSphere.startDmgSphere (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
	}
	void end_damage_sphere() {
		dmgSphere.endDmgSphere ();
	}
	void stateChange(string result){
		this.currentState = result;
		end_damage_sphere ();
	}
}
