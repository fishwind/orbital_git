﻿using UnityEngine;
using System.Collections;

public class Canvas_crosshair : MonoBehaviour {

	Animator anim;

	void Start () {
		anim = GetComponent <Animator> ();
	}
	
	void Update () {
	
	}

	public void playTriggerAnim(string animation) { anim.SetTrigger (animation); }
	public void resetTriggerAnim(string animation) { anim.ResetTrigger (animation); }
	public void setCrosshairActive(bool result) { GetComponent<Canvas> ().enabled = result; }
}
