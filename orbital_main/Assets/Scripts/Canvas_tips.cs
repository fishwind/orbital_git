﻿using UnityEngine;
using System.Collections;

public class Canvas_tips : MonoBehaviour {

	public GameObject[] tips_list;

	Animator anim;
	bool[] has_shown;

	void Start() {
		GetComponent<Canvas> ().worldCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
		anim = GetComponent<Animator> ();
		has_shown = new bool[tips_list.Length];
		for (int n = 0; n < has_shown.Length; n++) {
			has_shown [n] = false;
		}
	}

	public void showTips(int index) {
		if (!has_shown [index]) {
			foreach (GameObject tips in tips_list) {
				tips.SetActive (false);
			}
			tips_list [index].SetActive (true);
			has_shown [index] = true;
			anim.SetTrigger ("A_open");
		}
	}
	void closeTips() {
		anim.SetTrigger ("A_closed");
	}
}
