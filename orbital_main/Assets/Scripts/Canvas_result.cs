﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Canvas_result : MonoBehaviour {

	public Text[] stats_name;
	public Text[] stats_value;
	public Text mission_title;
	public Image currentRankImage;
	public GameObject rank_smoke;
	public Sprite[] rank_sprites;
	public GameObject option_retry;
	public GameObject option_change;

	Animator anim;
	int currentIndex = 0;
	float lerpSpeed = 19f;
	float lerpSpeedSlow = 2.7f;

	float[] currentStats;

	void Start() {
		currentStats = new float[11];
		anim = GetComponent<Animator> ();
		anim.SetTrigger ("A_open");
		StartCoroutine (showValues());
		if (GameManager.stats_missionSucceed) {
			mission_title.text = "MISSION COMPLETE!";
			option_retry.SetActive (false);
			option_change.SetActive (true);
		} else {
			mission_title.text = "MISSION FAIL!";
			option_retry.SetActive (true);
			option_change.SetActive (false);
		}
	}
	void Update() {
		
		currentStats[0] = Mathf.Lerp (currentStats[0], GameManager.stats_timeTaken, Time.deltaTime * lerpSpeed);
		currentStats[1] = Mathf.Lerp (currentStats[1], GameManager.stats_dmgTaken, Time.deltaTime * lerpSpeed);
		currentStats[2] = Mathf.Lerp (currentStats[2], GameManager.stats_manaUsed, Time.deltaTime * lerpSpeed);
		currentStats[3] = Mathf.Lerp (currentStats[3], GameManager.stats_enemiesKilled, Time.deltaTime * lerpSpeed);
		currentStats[4] = Mathf.Lerp (currentStats[4], GameManager.stats_itemCollected, Time.deltaTime * lerpSpeed);
		currentStats[5] = Mathf.Lerp (currentStats[5], GameManager.stats_numberButton, Time.deltaTime * lerpSpeed);
		currentStats[6] = Mathf.Lerp (currentStats[6], GameManager.stats_numberJump, Time.deltaTime * lerpSpeed);
		currentStats[7] = Mathf.Lerp (currentStats[7], GameManager.stats_numberAttack, Time.deltaTime * lerpSpeed);
		currentStats[8] = Mathf.Lerp (currentStats[8], GameManager.stats_numberDash, Time.deltaTime * lerpSpeed);
		currentStats[9] = Mathf.Lerp (currentStats[9], GameManager.stats_numberHurt, Time.deltaTime * lerpSpeed);
		currentStats[10] = Mathf.Lerp (currentStats[10], GameManager.totalScore, Time.deltaTime * lerpSpeedSlow);

		for (int n = 0; n < stats_value.Length; n++) {
			if (n != 10) {
				stats_value [n].text = ": " + ((int)(currentStats [n])).ToString ();
			} else {
				stats_value [n].text = "Total Score: " + ((int)(currentStats [n])).ToString ();
			}
		}
	}
	IEnumerator showValues() {
		GameManager.calculateResult ();
		switch (GameManager.rank) {
		case "SS": 
			currentRankImage.sprite = rank_sprites[0];
			break;
		case "S": 
			currentRankImage.sprite = rank_sprites[1];
			break;
		case "A": 
			currentRankImage.sprite = rank_sprites[2];
			break;
		case "B": 
			currentRankImage.sprite = rank_sprites[3];
			break;
		case "C": 
			currentRankImage.sprite = rank_sprites[4];
			break;
		case "D": 
			currentRankImage.sprite = rank_sprites[5];
			break;
		case "E": 
			currentRankImage.sprite = rank_sprites[6];
			break;
		case "F": 
			currentRankImage.sprite = rank_sprites[7];
			break;
		case "GG": 
			currentRankImage.sprite = rank_sprites[8];
			break;
		}
		yield return new WaitForSeconds (1);
		while(currentIndex < stats_value.Length) {
			currentStats [currentIndex] = 0f;
			stats_name [currentIndex].gameObject.SetActive (true);
			stats_value [currentIndex].gameObject.SetActive (true);
			currentIndex += 1;
			yield return new WaitForSeconds(0.25f);
		}

	}
	void play_rankSmoke() {
		rank_smoke.GetComponent<ParticleSystem> ().Stop ();
		rank_smoke.GetComponent<ParticleSystem> ().Play ();
	}
	public void shutup() {
		this.gameObject.SetActive (false);
		GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<AudioListener> ().enabled = false;
	}

}
