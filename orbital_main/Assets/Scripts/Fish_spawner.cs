﻿using UnityEngine;
using System.Collections;

public class Fish_spawner : MonoBehaviour {

	public GameObject fish_prefab;
	public int min_duplicate = 5;
	public int max_duplicate = 12;

	int duplicate;

	void Start () {
		duplicate = Random.Range (min_duplicate, max_duplicate);
		for (int n = 0; n < duplicate; n++) {
			Instantiate (fish_prefab, this.transform.position, Quaternion.identity);
		}
	}
}
