﻿using UnityEngine;
using System.Collections;

public class Boss_golem3 : MonoBehaviour {

	public GameObject boss_golem1;
		
	void boss_lookAtPlayerOnce() {
		boss_golem1.GetComponent<Boss_golem> ().lookAtPlayer_once ();
	}
	void boss_open_damage_sphere() {
		boss_golem1.GetComponent<Boss_golem> ().open_damage_sphere ();
	}
	void boss_end_damage_sphere() {
		boss_golem1.GetComponent<Boss_golem> ().end_damage_sphere ();
	}
	void boss_shootAcid() {
		boss_golem1.GetComponent<Boss_golem> ().shootAcid ();
	}
	void boss_spawnAcid() {
		boss_golem1.GetComponent<Boss_golem> ().spawnAcid ();
	}
	void boss_shootMetor() {
		boss_golem1.GetComponent<Boss_golem> ().shootMetor ();
	}
	void boss_start_hand_energy(){
		boss_golem1.GetComponent<Boss_golem> ().start_hand_energy ();
	}
	void boss_end_hand_energy(){
		boss_golem1.GetComponent<Boss_golem> ().end_hand_energy ();
	}
	void boss_disableSolidCollider() {
		boss_golem1.GetComponent<Boss_golem> ().disableSolidCollider ();
	}
	void boss_enableSolidCollider() {
		boss_golem1.GetComponent<Boss_golem> ().enableSolidCollider ();
	}
	void boss_disableHittableCollider() {
		boss_golem1.GetComponent<Boss_golem> ().disableHittableCollider ();
	}
	void boss_enableHittableCollider() {
		boss_golem1.GetComponent<Boss_golem> ().enableHittableCollider ();
	}
	void boss_playAudio(int index) {
		boss_golem1.GetComponent<Boss_golem> ().playAudio (index);
	}
}
