﻿using UnityEngine;
using System.Collections;

public class Shaker : MonoBehaviour {

	Vector3 originalPos;
	float shake_intensity = 0f;
	float shake_decay = 0f;

	void Start () {
		originalPos = transform.localPosition;
	}

	void Update () {
		if (shake_intensity > 0) {
			transform.localPosition = originalPos + Random.insideUnitSphere * shake_intensity;
			shake_intensity -= shake_decay;
		} else {
			transform.localPosition = originalPos;
		}
	}

	public void shake_big() {
		shake_intensity = 0.33f;
		shake_decay = 0.033f;
	}
	public void shake_small() {
		shake_intensity = 0.1f;
		shake_decay = 0.01f;
	}
}

