﻿using UnityEngine;
using System.Collections;

public class Harmless : MonoBehaviour {

	public EnemyHealth_manager health;
	public GameObject[] target_list;
	public float min_scale = 0.8f;
	public float max_scale = 1.2f;
	public float min_speed = 5f;
	public float max_speed = 9f;
	public float min_turnSpeed = 2f;
	public float max_turnSpeed = 8f;

	Animator anim;
	GameObject target;
	AudioController audioController;
	float move_speed;
	float turn_speed = 3f;
	bool isDead = false;

	void Awake() {
		health.enemyType = 1;
	}
	void Start() {
		audioController = GetComponent<AudioController> ();
		anim = GetComponent<Animator> ();
		//anim ["Take 001"].speed = 3f;
		target = target_list[Random.Range(0, target_list.Length)];
		move_speed = Random.Range (min_speed, max_speed);
		transform.localScale = transform.localScale * Random.Range (min_scale, max_scale);
	}

	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			} else {
				Vector3 vecDiff = target.transform.position - this.transform.position;
				transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (vecDiff), turn_speed * Time.deltaTime);
				transform.Translate (transform.InverseTransformDirection(transform.forward) * move_speed * Time.deltaTime);
				if (Vector3.Distance (this.transform.position, target.transform.position) <= 1f) {
					changeLocation ();
				}
			}
		}
	}

	void changeLocation() {
		target = target_list [Random.Range (0, target_list.Length)];
		move_speed = Random.Range (min_speed, max_speed);
		turn_speed = Random.Range (min_turnSpeed, max_turnSpeed);
	}
	void die() {
		anim.SetTrigger ("A_die");
		GameManager.stats_enemiesKilled += 1;
		GetComponent<Rigidbody> ().isKinematic = true;
		audioController.playOnce (0);
		gameObject.tag = "Untagged";
	}
	void clearBody() {
		GetComponent<Rigidbody> ().isKinematic = false;
		GetComponent<BoxCollider> ().enabled = false;
		Destroy (this.gameObject, 5f);
	}
}
