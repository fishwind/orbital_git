﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SphereCollider))]
public class Summoner_AI : MonoBehaviour {

	public float spawnDelay = 5f;
	public float spawnTimer = 3f;
	public float distanceFromPlayer = 100f; 	// distance away from player to start spawning.
	public bool canSpawn = false;
	public bool startingSpawn = true;
	public bool continuous = false;
	public GameObject[] spawnpoint_list;
	public GameObject[] AI_list;


	void Start() {
		GetComponent<SphereCollider> ().radius = distanceFromPlayer;
	}
	void Update () {
		if (canSpawn && continuous && Time.time > spawnTimer) {
			spawnContinuous ();
		}
	}
	void spawnOnce(){	// no. of initial spawn depends on no. spawnpoints
		foreach (GameObject spawnpoint in spawnpoint_list) {
			Instantiate (AI_list [Random.Range (0, AI_list.Length)], spawnpoint.transform.position, Quaternion.identity);
		}
	}
	void spawnContinuous() {
		Instantiate (AI_list [Random.Range (0, AI_list.Length)], spawnpoint_list [Random.Range (0, spawnpoint_list.Length)].transform.position, Quaternion.identity);
		spawnTimer = Time.time + spawnDelay;
	}
	void OnTriggerEnter(Collider hit) {
		if (hit.gameObject.tag == "Player") {
			canSpawn = true;
			GetComponent<SphereCollider> ().enabled = false;
			if (startingSpawn)
				spawnOnce ();
		}
	}
}
