﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Canvas_bossHP : MonoBehaviour {

	public EnemyHealth_manager health;
	//public Boss_AI boss;
	public Slider slider;
	public Image fill_image;
	public Slider slider_clone;
	public RectTransform particle_edgeHP;

	Animator anim;
	Color originialColor;
	bool hp_editable;
	float clone_speed = 1f;
	float color_speed = 15f;

	void Start() {
		anim = GetComponent<Animator> ();
		anim.SetTrigger ("A_startHP");
		hp_editable = false;
		originialColor = fill_image.color;
	}

	void Update() {
		if (hp_editable) {
			slider.value = (health.currentHealth/health.maxHealth);
			Vector2 pivot = particle_edgeHP.pivot;
			pivot.x = (health.currentHealth / health.maxHealth);
			particle_edgeHP.pivot = pivot;
		}
		slider_clone.value = Mathf.Lerp (slider_clone.value, slider.value, Time.deltaTime * clone_speed);
		fill_image.color = Color.Lerp (fill_image.color, originialColor, Time.deltaTime * color_speed);
	}

	public void flashHP() {
		fill_image.color = new Color (5,5,5);
	}

	void enable_hpEdit() {
		hp_editable = true;
		anim.enabled = false;
	}
	void disable_hpEdit() {
		hp_editable = false;
	}

}
