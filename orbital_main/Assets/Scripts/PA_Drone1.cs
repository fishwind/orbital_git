﻿using UnityEngine;
using System.Collections;

public class PA_Drone1 : MonoBehaviour {
	
	public EnemyHealth_manager health;
	public GameObject bulletPrefab;
	public GameObject explodeSelf;
	public float height_min = 4f;
	public float height_max = 15f;
	public float distance_min = -5f;
	public float distance_max = -35f;
	public float scale_min = 3;
	public float scale_max = 40;
	public float stoppingDistance_min = 0;
	public float stoppingDistance_max = 0;
	public float shootDelay_min = 2f;
	public float shootDelay_max = 3.5f;
	public float bullet_speed = 2000f;
	public float destroyDelay = 10f;

	GameObject player;
	GameObject body;	// to access spawnpoints easier
	float shootTimer = 3f;
	bool can_shoot = true;
	bool isDead = false;
	NavMeshAgent nav;
	AudioController audioController;
	ShakeCam shakeCam;

	void Awake() {
		health.enemyType = 2;
	}
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		nav = GetComponent<NavMeshAgent> ();
		body = transform.Find ("body").gameObject;
		nav.stoppingDistance = Random.Range(stoppingDistance_min, stoppingDistance_max);
		body.transform.localScale = body.transform.localScale * Random.Range(scale_min, scale_max);
		body.transform.localPosition = new Vector3 (body.transform.localPosition.x, Random.Range(height_min, height_max), Random.Range(distance_min, distance_max));
		audioController = GetComponent<AudioController> ();
		shakeCam = GetComponent<ShakeCam> ();
	}
		
	void Update () {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			} else if (can_shoot && Time.time > shootTimer) {
				shoot ();
			} else {
				nav.SetDestination (player.transform.position);
			}
		}
	}
	void shoot() {
		if(Vector3.Distance(player.transform.position, this.transform.position) <= 200f) {
			shootTimer = Time.time + Random.Range (shootDelay_min, shootDelay_max);
			Quaternion rotation = body.transform.Find("spawnpoint").transform.rotation;	// spawn bullet will have rotation = body's rotation
			GameObject bul = (GameObject)(Instantiate (bulletPrefab, body.transform.Find ("spawnpoint").transform.position, rotation));
			bul.GetComponent<Rigidbody> ().AddForce (bul.transform.forward * bullet_speed);
			audioController.playOnce (0);
		}
	}
		
	void die() {
		GameManager.stats_enemiesKilled += 1;
		GameObject explode = (GameObject)(Instantiate (explodeSelf, body.transform.position, Quaternion.identity));
		explode.transform.parent = body.gameObject.transform;
		Destroy (this.gameObject, destroyDelay);
		BoxCollider[] colliders = GetComponentsInChildren<BoxCollider> ();
		foreach (BoxCollider col in colliders) {
			col.enabled = false;
		}
		nav.enabled = false;
		GetComponent<Rigidbody> ().isKinematic = false;
		audioController.playOnce (1);
		shakeCam.shake_normal ();
	}
}
