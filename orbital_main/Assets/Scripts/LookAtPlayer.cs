﻿using UnityEngine;
using System.Collections;

public class LookAtPlayer : MonoBehaviour {

	public float followDelay = 0.015f;
	public bool hasDelay = false;

	GameObject player;

	void Start(){ 
		player = GameObject.FindGameObjectWithTag ("Player_lookme");
	}
	void FixedUpdate () {
		if (!hasDelay) {
			transform.LookAt (player.transform.position);
		} else {
			Vector3 vecDiff = player.transform.position - this.transform.position;
			this.transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.LookRotation(vecDiff), followDelay);
		}
	}
}
