﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Boss_AI_bullet : MonoBehaviour {

	public Boss_AI boss_AI;
	public GameObject bullet_prefab;
	public GameObject[] spawnpoint;
	public float shootDelay_high = 10f;
	public float shootDelay_mid = 8f;
	public float shootDelay_low = 6f;
	public float bullet_power = 9000f;

	Animator anim;
	float shootDelay = 10f;
	float shootTimer = 0f;
	string[] stringList = {"A_startBullet1", "A_startBullet2", "A_startBullet3"};

	void Start() {
		anim = GetComponent<Animator> ();
		shootDelay = shootDelay_high;
		shootTimer = Time.time + shootDelay;
	}
	void Update() {
		switch (boss_AI.getHealthLevel ()) {
		case "high":
			shootDelay = shootDelay_high;
			break;
		case "mid":
			shootDelay = shootDelay_mid;
			break;
		case "low":
			shootDelay = shootDelay_low;
			break;
		}
		if (!boss_AI.isDead && !boss_AI.isShootingMini && Time.time > shootTimer) {
			anim.SetTrigger (stringList[Random.Range(0, stringList.Length)]);
			shootTimer = Time.time + shootDelay;
			boss_AI.isShootingBullet = true;
		}
	}
	void shootBullet() {
		GameObject bul = (GameObject)(Instantiate (bullet_prefab, spawnpoint[Random.Range(0, spawnpoint.Length)].transform.position, transform.rotation));
		bul.GetComponent<Rigidbody>().AddForce(transform.forward * bullet_power);
	}
	public void end_bullet() {
		boss_AI.isShootingBullet = false;
		anim.SetTrigger ("A_idle");
	}
}
