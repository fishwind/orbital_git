﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Collectable : MonoBehaviour {

	Animator anim;
	float lifetime = 2000f;

	void Start() {
		anim = GetComponent<Animator> ();
		//Destroy (this.gameObject, lifetime);
	}

	void OnCollisionEnter(Collision hit) {
		if (hit.gameObject.tag == "ground") {
			//GetComponent<Rigidbody> ().useGravity = false;
			GetComponent<Rigidbody> ().isKinematic = true;
			anim.SetTrigger ("A_play");
		}
	}


}
