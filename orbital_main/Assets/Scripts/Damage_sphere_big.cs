﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Damage_sphere_big : MonoBehaviour {

	public float sphereDuration = 0.3f;
	public float hitSmall_power = 1000f;
	public float hit_dmg = 55f;
	public float startSize = 4f;
	public float endSize = 0.5f;
	public List<GameObject> hitList;

	float sphereTimer = 0;
	bool hasOpen = false;
	Vector3 starting;

	void Start() {
		starting = transform.localScale;
	}
	void OnTriggerEnter(Collider hit){
		if (!hitList.Contains (hit.gameObject)) {
			if(hit.gameObject.tag == "enemy_small") {
				hitList.Add (hit.gameObject);
				hit.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				hit.gameObject.GetComponent<Rigidbody>().AddExplosionForce(hitSmall_power, transform.position, 0, 2f);	// add force to hit object
			} else if (hit.gameObject.tag == "enemy_big") {
				hitList.Add (hit.gameObject);
				hit.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				hit.gameObject.GetComponent<Rigidbody>().AddExplosionForce(hitSmall_power *0.25f, transform.position, 0, 2f);	// add force to hit object
			}
		}
	}
	void Update() {
		if (hasOpen) {
			GetComponent<SphereCollider> ().radius = Mathf.Lerp (GetComponent<SphereCollider>().radius, endSize, sphereDuration);
			if (Time.time > sphereTimer) {
				hasOpen = false;
				closeSphere ();
			}
		}
	}
	public void openSphere(){	// call by player when attacking
		hitList.Clear ();
		GetComponent<SphereCollider> ().radius = startSize;
		GetComponent<SphereCollider> ().enabled = true;
		sphereTimer = Time.time + sphereDuration;
		hasOpen = true;
	}
	public void closeSphere(){
		GetComponent<SphereCollider> ().enabled = false;
		hitList.Clear ();
	}
}
