﻿using UnityEngine;
using System.Collections;

public class Buff_spawner : MonoBehaviour {
	
	public Transform[] spawnpoint;
	public GameObject[] buff_prefab;

	EnemyHealth_manager health;
	public float chance = 0.3f;
	bool hasDrop = false;

	void Start() {
		health = GetComponent<EnemyHealth_manager> ();
	}

	void LateUpdate() {
		if (hasDrop == false && health.currentHealth <= 0) {
			hasDrop = true;
			spawnBuff ();
		}
	}

	void spawnBuff() {
		foreach(Transform point in spawnpoint) {
			float value = Random.Range(0f, 1f);
			if(value <= chance) {
				GameObject buff = (GameObject)(Instantiate(buff_prefab[Random.Range(0, buff_prefab.Length)], point.position, Quaternion.identity));
				buff.GetComponent<Rigidbody>().AddExplosionForce(900f, this.transform.position, 30f, 15f);
			}
		}
	}

}
