﻿using UnityEngine;
using System.Collections;

public class PA_suicide_counter : MonoBehaviour {

	public GameObject mainBody;

	ShakeCam shakeCam;

	void Start() {
		shakeCam = GetComponent<ShakeCam> ();
	}

	void mainBody_explode() {
		mainBody.GetComponent<PA_suicide> ().die ();
		shakeCam.shake_normal ();
	}
}
