﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class Damage_sphere_small : MonoBehaviour {

	public float sphereDuration = 0.1f;
	public float hitSmall_power = 150f;
	public float hit_dmg = 10f;
	public List<GameObject> hitList;

	public GameObject spinParticle1_prefab;	// use Create/ Destroy to control
	public GameObject spinParticle1__big_prefab;	// use Create/ Destroy to control
	public GameObject currentParticle;
	public GameObject spinParticle2_prefab;	// use PLAY/ STOP to control
	public GameObject hitSpark_prefab;

	GameObject player;
	float sphereTimer = 0;
	bool hasOpen = false;
	AudioController audioController;

	void Start() {
		player = GameObject.FindGameObjectWithTag ("Player");
		audioController = GetComponent<AudioController> ();
		spinParticle2_prefab.GetComponent<ParticleSystem> ().Stop ();
		currentParticle = spinParticle1_prefab;
	}

	void OnTriggerEnter(Collider hit){
		if (!hitList.Contains (hit.gameObject)) {
			if(hit.gameObject.tag == "enemy_small") {
				audioController.playOnce (0);
				Instantiate(hitSpark_prefab, hit.transform.position, Quaternion.identity);
				hitList.Add (hit.gameObject);
				hit.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				hit.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * hitSmall_power);	// add force to hit object
			} else if (hit.gameObject.tag == "enemy_big") {
				audioController.playOnce (1);
				if (player.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("SAMK")) {
					Instantiate (hitSpark_prefab, player.transform.Find ("backLeg").transform.position, Quaternion.identity);
				} else {
					Instantiate (hitSpark_prefab, player.transform.Find ("frontLeg").transform.position, Quaternion.identity);
				}
				hitList.Add (hit.gameObject);
				hit.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				player.GetComponent<Rigidbody>().AddExplosionForce(200f, hit.transform.position, 0, 5f);
			} else if (hit.gameObject.tag == "enemy_boss") {
				audioController.playOnce (1);
				if (player.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("SAMK")) {
					Instantiate (hitSpark_prefab, player.transform.Find ("backLeg").transform.position, Quaternion.identity);
				} else {
					Instantiate (hitSpark_prefab, player.transform.Find ("frontLeg").transform.position, Quaternion.identity);
				}
				hitList.Add (hit.gameObject);
				hit.GetComponentInParent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				player.GetComponent<Rigidbody> ().AddRelativeForce (Vector3.up * 200f);
			} 
		}
	}
	void Update() {
		if (hasOpen && Time.time > sphereTimer) {
			hasOpen = false;
			closeSphere ();
		}
	}
	public void openSphere(){	// call by player when attacking
		hitList.Clear ();
		GetComponent<SphereCollider> ().enabled = true;
		sphereTimer = Time.time + sphereDuration;
		GameObject spin1 = (GameObject)(Instantiate (currentParticle, transform.position, Quaternion.identity));
		spin1.transform.parent = this.transform;
		spinParticle2_prefab.GetComponent<ParticleSystem> ().Play ();
		hasOpen = true;
	}

	public void closeSphere(){
		GetComponent<SphereCollider> ().enabled = false;
		spinParticle2_prefab.GetComponent<ParticleSystem> ().Stop ();
		hitList.Clear ();
	}
}
