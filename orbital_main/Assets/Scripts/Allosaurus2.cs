﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Allosaurus2 : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageSphere dmgSphere;
	public float dashPower = 1000f;
	public float close_enough_distance = 50f;
	public float tooClose_distance = 3.5f;
	public float dashArea = 30f;
	public float dmgSphere_amount = 200f;
	public float dmgSphere_force = 1500;
	public float dmgSphere_forceUp = 2f;
	public bool dmgSphere_isBig = true;	// big sphere trigger player's hurtBig animation

	NavMeshAgent nav;
	Animator anim;
	AudioController audioController;
	GameObject player;
	bool isDead = false;
	bool isChasing = true;
	bool inside_dashArea = false;

	void Awake() {
		health.enemyType = 2;
	}
	void Start() {
		nav = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		InvokeRepeating ("charging_state",6f , 5f);
	}
	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die_state ();
			} else {
				float distance = Vector3.Distance (this.transform.position, player.transform.position);
				if (isChasing && distance < close_enough_distance) {
					if (distance > tooClose_distance) {
						chase_state ();
					} else {
						waiting_state ();
					}
				}
			}
		}
	}
	void FixedUpdate() {
		float distanceDifferent = Vector3.Distance (this.transform.position, player.transform.position);
		if (distanceDifferent < dashArea) {
			inside_dashArea = true;
		} else {
			inside_dashArea = false;
		}
	}
	void chase_state() {
		anim.SetBool ("A_run", true);
		anim.SetBool ("A_waiting", false);
		nav.SetDestination (player.transform.position);
	}
	void waiting_state() {
		anim.SetBool ("A_run", false);
		anim.SetBool ("A_waiting", true);
		nav.SetDestination (player.transform.position);
	}

	void charging_state() {
		if (isDead == false && inside_dashArea && isChasing) {
			isChasing = false;
			anim.SetBool ("A_run", false);
			anim.SetBool ("A_waiting", false);
			GetComponent<LookAtPlayer> ().enabled = true;
			anim.SetTrigger ("A_charge");
		}
	}
	void attack_state() {
		GetComponent<Rigidbody> ().drag = 0;
		audioController.playOnce (1);
		anim.SetTrigger ("A_attack");
		dmgSphere.startDmgSphere (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
		GetComponent<Rigidbody> ().AddRelativeForce (Vector3.forward * dashPower);
	}
	void recover_state() {	// recover from attacking
		isChasing = true;
		anim.SetBool("A_run", true);
		anim.SetBool ("A_waiting", false);
		GetComponent<Rigidbody>().drag = 2f;
		GetComponent<LookAtPlayer> ().enabled = true;
		dmgSphere.endDmgSphere ();
	}
	void die_state() {
		audioController.playOnce (2);
		anim.SetBool ("A_run", false);
		anim.SetBool ("A_waiting", false);
		anim.SetTrigger ("A_die");
		GetComponent<LookAtPlayer> ().enabled = false;
	}
	void clearBody() {
		BoxCollider[] colliders = GetComponentsInChildren<BoxCollider> ();
		foreach (BoxCollider col in colliders) {
			col.enabled = false;
		}
		GameManager.stats_enemiesKilled += 1;
		nav.enabled = false;
		Destroy (this.gameObject, 5f);
	}
	void offFollowPlayer() {
		GetComponent<LookAtPlayer> ().enabled = false;
	}
}