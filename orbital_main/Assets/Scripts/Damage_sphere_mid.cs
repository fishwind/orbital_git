﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class Damage_sphere_mid : MonoBehaviour {

	public float sphereDuration = 0.4f;
	public float hitSmall_power = 550f;
	public float hit_dmg = 15f;
	public List<GameObject> hitList;
	public GameObject hitSpark_prefab;

	GameObject player;
	float sphereTimer = 0;
	bool hasOpen = false;
	AudioController audioController;

	void Start() {
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	void OnTriggerEnter(Collider hit){
		if (!hitList.Contains (hit.gameObject)) {
			if(hit.gameObject.tag == "enemy_small") {
				audioController.playOnce (0);
				Instantiate(hitSpark_prefab, hit.transform.position, Quaternion.identity);
				hitList.Add (hit.gameObject);
				hit.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				hit.gameObject.GetComponent<Rigidbody>().AddExplosionForce(hitSmall_power, transform.position, 0, 0.5f);	// add force to hit object
			} else if (hit.gameObject.tag == "enemy_big") {
				audioController.playOnce (1);
				Instantiate(hitSpark_prefab, hit.transform.position, Quaternion.identity);
				hitList.Add (hit.gameObject);
				hit.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				hit.gameObject.GetComponent<Rigidbody>().AddExplosionForce(hitSmall_power*0.25f, transform.position, 0, 0.5f);	// add force to hit object
			} else if (hit.gameObject.tag == "enemy_boss") {
				audioController.playOnce (1);
				if (player.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("SAMK")) {
					Instantiate (hitSpark_prefab, player.transform.Find ("backLeg").transform.position, Quaternion.identity);
				} else {
					Instantiate (hitSpark_prefab, player.transform.Find ("frontLeg").transform.position, Quaternion.identity);
				}
				hitList.Add (hit.gameObject);
				hit.GetComponentInParent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				player.GetComponent<Rigidbody> ().AddRelativeForce (Vector3.up * 327f);
			} 
		}
	}
	void Update() {
		if (hasOpen && Time.time > sphereTimer) {
			hasOpen = false;
			closeSphere ();
		}
	}
	public void openSphere(){	// call by player when attacking
		hitList.Clear ();
		GetComponent<SphereCollider> ().enabled = true;
		sphereTimer = Time.time + sphereDuration;
		hasOpen = true;
	}
	public void closeSphere(){
		GetComponent<SphereCollider> ().enabled = false;
		hitList.Clear ();
	}
}