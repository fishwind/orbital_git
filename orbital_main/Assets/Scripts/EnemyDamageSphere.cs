﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class EnemyDamageSphere : MonoBehaviour {

	float damageAmount = 0f;
	float forceAmount = 100f;
	float throwUp = 3f;
	public bool is_big = true;
	bool hasHit = false;

	void OnTriggerEnter(Collider hit) {
		if (hasHit == false && hit.gameObject.tag == "Player") {
			hasHit = true;
			// dmg player by damageAmount;
			if (is_big) {
				hit.gameObject.GetComponent<PlayerController> ().player_hurt_big ();
				hit.gameObject.GetComponent<Rigidbody> ().AddExplosionForce (forceAmount, this.transform.position, 0, throwUp);
			} else {
				hit.gameObject.GetComponent<PlayerController> ().player_hurt_small ();
			}
		}
	}

	public void startDmgSphere(float dmgAmt, float forceAmt, float up, bool isBig, bool hitAlr) {
		GetComponent<SphereCollider> ().enabled = true;
		this.damageAmount = dmgAmt;
		this.forceAmount = forceAmt;
		this.throwUp = up;
		this.is_big = isBig;
		this.hasHit = hitAlr;
	}
	public void endDmgSphere() {
		GetComponent<SphereCollider> ().enabled = false;
	}
}