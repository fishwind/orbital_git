﻿using UnityEngine;
using System.Collections;

public class CharacterSelection_controller : MonoBehaviour {

	public GameObject moveable;
	public GameObject[] unitychans;
	public int index = 0;

	string[] unitychan_animations = {"pose_1","pose_2","pose_3","pose_4","pose_5","pose_6","pose_7","pose_8","pose_9","pose_10","pose_11" };	// trigger parameters
	string[] unitychan_animations2 = {"pose_2", "pose_5","pose_6", "pose_8","pose_9", "pose_11" };	// trigger parameters
	float translateAmount = 25f;
	float targetPosition_x;
	float translateSpeed = 10f;

	void Start() {
		index = 0;
	}
	void FixedUpdate() {
		GetComponent<Rigidbody> ().AddForce (Vector3.up * -1 * 50f);
	}
	void Update() {
		moveable.transform.localPosition = Vector3.Lerp (moveable.transform.localPosition, new Vector3 (targetPosition_x, moveable.transform.localPosition.y, moveable.transform.localPosition.z), Time.deltaTime * translateSpeed);
	}
	public void switchCharacter_right() {
		index = (index + 1 + unitychans.Length) % unitychans.Length;
		targetPosition_x = index * translateAmount;
		Animator animz = unitychans [index].GetComponent<Animator> ();
		if (animz.runtimeAnimatorController.name == "menu_unitychan_poses") {
			animz.SetTrigger (unitychan_animations [Random.Range (0, unitychan_animations.Length)]);
		} else {
			animz.SetTrigger (unitychan_animations2 [Random.Range (0, unitychan_animations2.Length)]);
		}
	}
	public void switchCharacter_left() {
		index = (index - 1 + unitychans.Length) % unitychans.Length;
		targetPosition_x = index * translateAmount;
		Animator animz = unitychans [index].GetComponent<Animator> ();
		if (animz.runtimeAnimatorController.name == "menu_unitychan_poses") {
			animz.SetTrigger (unitychan_animations [Random.Range (0, unitychan_animations.Length)]);
		} else {
			animz.SetTrigger (unitychan_animations2 [Random.Range (0, unitychan_animations2.Length)]);
		}	
	}
	void OnCollisionEnter(Collision hit) {
		if (hit.gameObject.tag == "ground") {
			unitychans [index].GetComponent<Animator> ().SetTrigger ("pose_5");
		}
	}
}
