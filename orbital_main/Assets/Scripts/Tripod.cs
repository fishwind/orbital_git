﻿using UnityEngine;
using System.Collections;

public class Tripod : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageLine dmgLine;
	public GameObject spawnpoint;
	public GameObject wave;
	public GameObject bullet_laser;
	public LookAtPlayer lookAtMe;
	public tracker track;


	public float chase_distance = 200f;
	public float random_distance = 45f;
	public float move_speed = 4f;
	public float dmgLine_amount = 200f;
	public float dmgLine_force = 1500;
	public float dmgLine_forceUp = 0.5f;
	public bool dmgLine_isBig = true;

	Animator anim;
	AudioController audioController;
	GameObject player;
	GameObject laser_prefab1 = null;	// to be used for laser only
	Vector3 randomDir;
	float distance;
	bool isShooting = false;

	void Awake() {
		health.enemyType = 4;
	}

	void Start() {
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		float shootCd = Random.Range (8f, 10f);
		InvokeRepeating ("startShoot", 5f, shootCd);
		InvokeRepeating ("calculateDis", 3f, 4f);
		randomVec ();
		calculateDis ();
		audioController.playLoop (2);
	}

	void Update() {
		if (health.currentHealth <= 0 && !anim.GetNextAnimatorStateInfo(0).IsName("tripoddeath")) {
			anim.SetBool ("A_run", false);
			anim.SetBool ("A_shoot", false);
			die ();
		} else { 
			move ();
		}
	}
	void move() {
		if (distance <= chase_distance && track.sawPlayer) {
			if (isShooting) {
				anim.SetBool ("A_run", false);
				anim.SetBool ("A_shoot", true);
			} else {
				anim.SetBool ("A_run", true);
				anim.SetBool ("A_shoot", false);
				if (distance >= random_distance) {
					move_speed = 15f;
					Vector3 vecDiff = player.transform.position - this.transform.position;
					vecDiff.y = 0f;
					vecDiff.Normalize ();
					transform.Translate (vecDiff * Time.deltaTime * move_speed);
					float angle = Quaternion.LookRotation (vecDiff).y;
					anim.SetFloat ("dir", angle);
				} else {
					move_speed = 6f;
					transform.Translate (randomDir * Time.deltaTime * move_speed);
					float angle = Quaternion.LookRotation (randomDir).y;
					anim.SetFloat ("dir", angle);
				}
			}
		}
	}
	void calculateDis() {
		distance = Vector3.Distance (this.transform.position, player.transform.position);
	}
	void randomVec() {
		randomDir = new Vector3 (Random.Range (-1f, 1f), 0, Random.Range (-1f, 1f));
	}
	void startShoot() {
		isShooting = true;
		audioController.stopAudio ();
	}
	void endShoot() {
		audioController.playLoop (2);
		isShooting = false;
	}
	void shoot() {
		randomVec ();
		startDmg ();

		GameObject wav1 = (GameObject)Instantiate (wave, spawnpoint.transform.position, spawnpoint.transform.rotation);
		wav1.transform.Rotate (Vector3.left, 90.0f);
		wav1.GetComponent<BeamWave> ().col = this.GetComponent<BeamParam> ().BeamColor;

		laser_prefab1 = (GameObject)Instantiate (bullet_laser, spawnpoint.transform.position, spawnpoint.transform.rotation);

		BeamParam bp = GetComponent<BeamParam> ();
		if (laser_prefab1.GetComponent<BeamParam> ().bGero)
			laser_prefab1.transform.parent = spawnpoint.transform;
		Vector3 s = new Vector3 (bp.Scale, bp.Scale, bp.Scale);

		laser_prefab1.transform.localScale = s;
		laser_prefab1.GetComponent<BeamParam> ().SetBeamParam (bp);

		audioController.playOnce (1);
	}
	void stop() {
		endDmg ();
		if (laser_prefab1 != null) {
			laser_prefab1.GetComponent<BeamParam> ().bEnd = true;
		}
	}
	void startDmg() {
		dmgLine.startDmgLine (dmgLine_amount, dmgLine_force, dmgLine_forceUp, dmgLine_isBig, false);
	}
	void endDmg() {
		dmgLine.endDmgLine ();
	}
	void die() {
		health.gameObject.tag = "Untagged";
		anim.SetTrigger ("A_die");
		if (lookAtMe.enabled == true) {
			stop ();
			audioController.stopAudio ();
			audioController.playOnce (0);
		}
		lookAtMe.enabled = false;
	}
	void clearbody() {
		GameManager.stats_enemiesKilled += 1;
		Collider[] colliders = GetComponentsInChildren<Collider> ();
		foreach (Collider col in colliders) {
			col.enabled = false;
		}
		Destroy (this.gameObject, 5f);
	}
}
