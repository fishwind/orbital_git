﻿using UnityEngine;
using System.Collections;

public class Butterfly_spawner : MonoBehaviour {

	public GameObject butterfly_prefab;
	int duplicate;

	void Start () {
		duplicate = Random.Range (1, 7);
		for (int n = 0; n < duplicate; n++) {
			Instantiate (butterfly_prefab, this.transform.position, Quaternion.identity);
		}
	}
}
