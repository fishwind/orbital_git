﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Boss_AI_mini : MonoBehaviour {

	public Boss_AI boss_AI;
	public GameObject[] mini_prefab;
	public GameObject[] spawnpoint;
	public GameObject shootParticle;
	public GameObject mini_storage;
	public float shootDelay_high = 25f;
	public float shootDelay_mid = 15f;
	public float shootDelay_low = 10f;
	public float bullet_power = 300f;

	Animator anim;
	string[] stringList = { "A_startMini1", "A_startMini2" };
	float shootDelay = 0f;
	float shootTimer = 0;

	void Start() {
		anim = GetComponent<Animator> ();
		anim.enabled = false;
		shootDelay = shootDelay_high;
		shootTimer = Time.time + shootDelay;
	}

	void Update() {
		switch (boss_AI.getHealthLevel ()) {
		case "high":
			shootDelay = shootDelay_high;
			break;
		case "mid":
			shootDelay = shootDelay_mid;
			break;
		case "low":
			shootDelay = shootDelay_low;
			break;
		}
		if (!boss_AI.isDead && !boss_AI.isShootingBullet && !boss_AI.isShootingLazer && Time.time > shootTimer) {
			anim.enabled = true;
			shootTimer = Time.time + shootDelay;
			boss_AI.isShootingMini = true;
			if (boss_AI.getHealthLevel () == "high" || boss_AI.getHealthLevel () == "mid") {
				anim.SetTrigger (stringList [0]);
			} else {
				anim.SetTrigger (stringList [1]);
			}
		}
	}
	void shootMini() {
		shootParticle.GetComponent<ParticleSystem> ().Stop ();
		shootParticle.GetComponent<ParticleSystem> ().Play ();
		GameObject bul = (GameObject)(Instantiate (mini_prefab[Random.Range(0, mini_prefab.Length)], spawnpoint[Random.Range(0, spawnpoint.Length)].transform.position, transform.rotation));
		bul.GetComponent<Rigidbody>().AddExplosionForce(bullet_power, transform.position, 150f);
		bul.transform.parent = mini_storage.transform;
	}
	public void endMini() {
		boss_AI.isShootingMini = false;
		anim.SetTrigger ("A_idle");
		anim.enabled = false;
	}
}
