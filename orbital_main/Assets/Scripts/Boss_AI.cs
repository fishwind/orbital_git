﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Boss_AI : MonoBehaviour {

	public Boss_AI_lazer boss_ai_lazerL;
	public Boss_AI_lazer boss_ai_lazerR;
	public Boss_AI_bullet boss_ai_bullet;
	public Boss_AI_mini boss_ai_mini;
	public Boss_AI_missile boss_ai_missile;
	public EnemyHealth_manager health;
	public GameObject particle_hurt;
	public GameObject[] particle_die;
	public GameObject particle_burn;
	public GameObject mini_storage;
	public bool isDead = false;
	public bool isShootingLazer = false;
	public bool isShootingBullet = false;
	public bool isShootingMini = false;
	public bool isShootingMissile = false;

	GameObject player;
	AudioController audioController;
	Animator anim;
	Level_last_manager levelManager;
	string health_level = "high";	// "mid" & "low"
	float timeScale_speed = 2f;
	bool can_timeScale = false;

	void Awake() {
		health.enemyType = 5;
	}
	void Start() {
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		levelManager = GameObject.FindGameObjectWithTag ("levelManager").GetComponent<Level_last_manager>();
	}
	void Update() {
		if (can_timeScale) {
			Time.timeScale = Mathf.MoveTowards (Time.timeScale, 1f, Time.deltaTime * timeScale_speed);
		}
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				dieEffects ();
			} else if ((health.currentHealth / health.maxHealth) < 0.7f && (health.currentHealth / health.maxHealth) >= 0.2f) {
				if (health_level != "mid") midEffects ();
				health_level = "mid";
			} else if ((health.currentHealth / health.maxHealth) < 0.2f) {
				if (health_level != "low") lowEffects ();
				health_level = "low";
			}
		}
		if (Input.GetKeyDown ("g"))
			dieEffects ();
	}

	public string getHealthLevel() { return this.health_level; }

	/*
	void recover() {
		anim.SetTrigger ("A_idle");
	}*/

	public void hurtEffects() {
		particle_hurt.GetComponent<ParticleSystem> ().Stop ();
		particle_hurt.GetComponent<ParticleSystem> ().Play ();
	}
	void midEffects() {
		var em = particle_burn.GetComponent<ParticleSystem> ().emission;
		var rate = new ParticleSystem.MinMaxCurve ();
		rate.constantMax = 5f;
		em.rate = rate;
	}
	void lowEffects() {
		var em = particle_burn.GetComponent<ParticleSystem> ().emission;
		var rate = new ParticleSystem.MinMaxCurve ();
		rate.constantMax = 24f;
		em.rate = rate;
	}
	void dieEffects() {
		isDead = true;
		Destroy (mini_storage.gameObject);
		boss_ai_lazerL.GetComponent<Boss_AI_lazer> ().endLazer ();
		boss_ai_lazerR.GetComponent<Boss_AI_lazer> ().endLazer ();
		boss_ai_bullet.GetComponent <Boss_AI_bullet> ().end_bullet ();
		boss_ai_mini.GetComponent<Boss_AI_mini> ().endMini ();
		boss_ai_missile.GetComponent<Boss_AI_missile> ().endMissile ();
		anim.SetTrigger ("A_die");
		foreach (GameObject particle in particle_die) {
			particle.GetComponent<ParticleSystem> ().Play ();
		}
		can_timeScale = true;
		Time.timeScale = 0.05f;
		// dark screen
		gameOver();
	}
	public void gameOver() {
		levelManager.gameOver ();
	}
}
