﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class STARTER : MonoBehaviour {

	public GameObject camera_starter;
	public GameObject camera_jumping;
	public GameObject directional_arrow_first;
	public Transform spawnpoint_playerJump;
	public List<GameObject> UI_to_be_enabled;


	GameObject camera_main;
	GameObject player;
	AudioController audioController;
	PlayerController playerController;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerController = player.GetComponent<PlayerController> ();
		playerController.enableMove = false;
		camera_main = GameObject.FindGameObjectWithTag ("MainCamera");
		camera_main.GetComponent<Camera> ().enabled = false;
		camera_main.GetComponent<AudioListener> ().enabled = false;
		directional_arrow_first.SetActive (false);
		audioController = GetComponent<AudioController> ();
	}
	
	void Update () {
		/*------------------------------------------------------------------------------*/	// testing controls
		if (Input.GetKeyDown ("p")) {	// move char to position, jump off, switch camera
			//jumpOff();
		}
		if (Input.GetKeyDown ("o")) {
			//fallOff ();
		}
		if (Input.GetKeyDown ("i")) {
			//startGame ();
		}
		/*------------------------------------------------------------------------------*/

	}

	void jumpOff() {
		player.transform.position = spawnpoint_playerJump.transform.position;
		playerController.move_helicoperJump ();
	}

	void fallOff() {
		//camera_jumping.SetActive (false);
		camera_main.GetComponent<Camera> ().enabled = true;
		camera_main.GetComponent<AudioListener> ().enabled = true;
	}

	void setFog() {
		if (GameManager.chosen_level == 2) {
			RenderSettings.fogColor = new Color (0f / 255f, 0f / 255f, 159f / 255f);
		}
	}

	IEnumerator startGame() {	// open all UI, play sound
		yield return new WaitForSeconds(6.3f);
		if (GameManager.chosen_level == 1) {
			GameObject.FindGameObjectWithTag ("levelManager").GetComponent<AudioController> ().playLoop (3);
		} else if (GameManager.chosen_level == 2) {
			GameObject.FindGameObjectWithTag ("levelManager").GetComponent<AudioController> ().playLoop (5);
		}
		directional_arrow_first.SetActive (true);
		playerController.enableMove = true;
		UI_to_be_enabled.Add (playerController.canvas_bars.gameObject);
		UI_to_be_enabled.Add (camera_main.GetComponent<Camera_Controller1>().directional_arrow.gameObject);
		if (GameManager.chosen_level == 2) {
			UI_to_be_enabled.Add (playerController.canvas_icon.gameObject);
		}
		foreach (GameObject ui in UI_to_be_enabled) {
			ui.SetActive (true);
		}
	}

	void countdown_five() {
		playerController.canvas_tips.showTips (7);
		audioController.playOnce (1);
		StartCoroutine (startGame());
	}
}
