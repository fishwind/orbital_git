﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Boss_golem : MonoBehaviour {

	public EnemyHealth_manager health;
	public GameObject bodySkin;
	public Animator anim_1;
	public Animator anim_2;
	public Animator anim_3;
	public GameObject[] tele_places;
	public GameObject[] acid;
	public GameObject[] handEnergy;
	public GameObject[] dieParticles;
	public GameObject particle_summon;
	public Collider[] colliderList;
	public GameObject boss_golem1;
	public GameObject water;
	public GameObject acid_prefab;
	public GameObject spawnpoint_acid;
	public GameObject metor_prefab;
	public GameObject zombie_prefab;
	public GameObject spawnpoint_summon;
	public GameObject spawnpoint_metor;
	public List<GameObject> zombie_parent;
	public Buff_spawner_golem buff_spawner;
	public EnemyDamageLine handDmgCube_L;
	public EnemyDamageLine handDmgCube_R;
	public EnemyDamageLine bodyDmgCube;
	public float closeEnough_short = 100f;
	public float closeEnough_long = 500f;
	public float metorSpeed = 3000f;
	public float dmgSphere_amount = 500f;
	public float dmgSphere_force = 5000;
	public float dmgSphere_forceUp = 1f;
	public bool dmgSphere_isBig = true;
	public float cd_low1 = 5f;
	public float cd_low2 = 7f;
	public float cd_mid1 = 7f;
	public float cd_mid2 = 9f;
	public float cd_high1 = 9f;
	public float cd_high2 = 11f;
	public bool isDead = false;

	Level_1_manager levelManager;
	GameObject player;
	AudioController audioController;
	LookAtPlayer lookAtPlayer; 
	Color defaultColor;
	Color hurtColor;
	float colorTime = 14f;
	int[] list;						// 1: dig			2: spin/metor			3: spin/acid			4: acid/metor			5: summon
	int[] list_high = {1, 2};
	int[] list_mid = {1, 1, 1, 2, 3, 4};
	int[] list_low = {1, 1, 1, 2, 3, 4, 5, 5};
	float tip_timer = 20f;
	string health_level = "high";
	int lastIndex = 0;
	float waterTimer = 0f;
	float timer = 0f;
	float cd1;
	float cd2;

	void Start() {
		//water.transform.parent = this.transform;
		//defaultColor = new Color (204f / 255f, 204f / 255f, 204f / 255f);
		//defaultColor = new Color (35f / 255f, 35f / 255f, 35f / 255f);
		defaultColor = new Color (255 / 255f, 249 / 255f, 196f / 255f);

		hurtColor = new Color (1.5f, 1.5f, 1.5f);
		//hurtColor = new Color (100f / 255f, 100f / 255f, 100f / 255f);
		//hurtColor = new Color (150f / 255f, 150f / 255f, 150f / 255f);

		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		levelManager = GameObject.FindGameObjectWithTag ("levelManager").GetComponent<Level_1_manager>();
		lookAtPlayer = GetComponent<LookAtPlayer> ();
		cd1 = cd_high1;
		cd2 = cd_high2;
		list = list_high;
		timer = Time.time + newCd ();
		waterTimer = Time.time + 3f;
	}

	void Update() {
		tip_timer -= Time.deltaTime;
		if (tip_timer <= 0) {
			player.GetComponent<PlayerController> ().canvas_tips.showTips (8);
		}
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				dieEffects ();
			} else if ((health.currentHealth / health.maxHealth) < 0.85f && (health.currentHealth / health.maxHealth) >= 0.4f) {
				if (health_level != "mid") midEffects ();
				health_level = "mid";
			} else if ((health.currentHealth / health.maxHealth) < 0.4f) {
				if (health_level != "low") lowEffects ();
				health_level = "low";
			}
			bodySkin.GetComponent<SkinnedMeshRenderer> ().material.SetColor("_Color", Color.Lerp (bodySkin.GetComponent<SkinnedMeshRenderer> ().material.GetColor ("_Color"), defaultColor, colorTime * Time.deltaTime));
			switch (health_level) {
			case "high": 
				cd1 = cd_high1;
				cd2 = cd_high2;
				list = list_high;
				break;
			case "mid": 
				cd1 = cd_mid1;
				cd2 = cd_mid2;
				list = list_mid;
				break;
			case "low": 
				cd1 = cd_low1;
				cd2 = cd_low2;
				list = list_low;
				break;
			}
			if(health_level != " " && Time.time > waterTimer && anim_3.GetCurrentAnimatorStateInfo(0).IsName("Boss_golem_idle") && anim_2.GetCurrentAnimatorStateInfo(0).IsName("Boss_golem_float")) {
				waterTimer = Time.time + 60f;
				timer = Time.time + 8f;	// after 8sec, water is stable for other animation
				anim_1.SetTrigger("A_seaLevel");
				anim_2.SetTrigger("A_seaLevel");
				anim_3.SetTrigger ("A_seaLevel");
			}
			if (Time.time > timer) {
				timer = Time.time + newCd ();
				move (list);
			}
			 if (Input.GetKeyDown ("g")) dieEffects ();
		}
	}

	float newCd() {
		return Random.Range (cd1, cd2);
	}

	void move(int[] lst) {
		if(anim_3.GetCurrentAnimatorStateInfo(0).IsName("Boss_golem_idle")) {
			waterTimer += 2f;
			int methodType = lst [Random.Range (0, lst.Length)];
			switch (methodType) {
			case 1:
				util1 ();
				break;
			case 2: 
				attack1 ();
				break;
			case 3:
				attack2 ();
				break;
			case 4:
				attack3 ();
				break;
			case 5:
				util2 ();
				break;
			}
		}
	}

	void util1() {
		anim_2.SetTrigger ("A_dig");
		anim_3.SetTrigger ("A_dig");
	}
	void attack1() {
		Vector3 distanceDiff = player.transform.position - this.transform.position;
		//print (distanceDiff.magnitude);
		if (distanceDiff.magnitude <= closeEnough_short) {
			anim_2.SetTrigger ("A_spin");
			timer = timer + 2f;
		} else {
			anim_3.SetTrigger ("A_metor");
			anim_2.SetTrigger ("A_metor");
		}
	}
	void attack2() {
		Vector3 distanceDiff = player.transform.position - this.transform.position;
		if (distanceDiff.magnitude <= closeEnough_short) {
			anim_2.SetTrigger ("A_spin");
			timer = timer + 2f;
		} else {
			anim_3.SetTrigger ("A_acid");
		}
	}
	void attack3() {
		Vector3 distanceDiff = player.transform.position - this.transform.position;
		if (distanceDiff.magnitude <= closeEnough_short) {
			anim_3.SetTrigger ("A_acid");
		} else {
			anim_3.SetTrigger ("A_metor");
			anim_2.SetTrigger ("A_metor");		}
	}
	void util2() {
		anim_3.SetTrigger ("A_summon");
		anim_2.SetTrigger ("A_summon");
		GameObject package = (GameObject)(Instantiate (zombie_prefab, spawnpoint_summon.transform.position, Quaternion.identity));
		zombie_parent.Add (package);
		particle_summon.GetComponent<ParticleSystem> ().Stop ();
		particle_summon.GetComponent<ParticleSystem> ().Play ();
	}

	public void lookAtPlayer_once() {
		Vector3 vecDiff = player.transform.position - this.transform.position;
		vecDiff.y = 0f;
		transform.rotation = Quaternion.LookRotation (vecDiff);
	}
	public string getHealthLevel() { return this.health_level; }

	public void teleport() {
		int currentIndex = Random.Range (0, tele_places.Length);
		if (currentIndex == lastIndex) {
			this.transform.position = tele_places [(currentIndex + 1) % tele_places.Length].transform.position;
		} else {
			this.transform.position = tele_places [currentIndex].transform.position;
		}
		lastIndex = currentIndex;
		anim_3.SetTrigger ("A_idle");
		lookAtPlayer_once ();
	}
	public void shootAcid() {
		foreach (GameObject eachAcid in acid) {
			eachAcid.GetComponent<ParticleSystem> ().Stop ();
			eachAcid.GetComponent<ParticleSystem> ().Play ();
		}
	}
	public void spawnAcid()  {
		Instantiate (acid_prefab, spawnpoint_acid.transform.position, Quaternion.identity);
	}
	public void shootMetor()  {
		GameObject metor = (GameObject)(Instantiate (metor_prefab, spawnpoint_metor.transform.position, spawnpoint_metor.transform.rotation));
		metor.GetComponent<Rigidbody> ().AddForce (spawnpoint_metor.transform.forward * metorSpeed);
	}
	public void open_damage_sphere() {
		handDmgCube_L.startDmgLine (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
		handDmgCube_R.startDmgLine (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
		bodyDmgCube.startDmgLine (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
	}
	public void end_damage_sphere() {
		handDmgCube_L.endDmgLine ();
		handDmgCube_R.endDmgLine ();
		bodyDmgCube.endDmgLine ();
	}
	public void start_hand_energy() {
		foreach (GameObject energy in handEnergy) {
			ParticleSystem.EmissionModule em = energy.GetComponent<ParticleSystem> ().emission;
			em.enabled = true;	
		}
	}
	public void end_hand_energy() {
		foreach (GameObject energy in handEnergy) {
			ParticleSystem.EmissionModule em = energy.GetComponent<ParticleSystem> ().emission;
			em.enabled = false;	
		}
	}
	public void disableSolidCollider() {	// prevent golem hitting u fly away
		foreach(Collider collider in colliderList) {
			collider.enabled = false;
		}
	}
	public void enableSolidCollider() {
		foreach(Collider collider in colliderList) {
			collider.enabled = true;
		}
	}
	public void disableHittableCollider() {	// prevent player hitting golem 
		foreach(Collider collider in colliderList) {
			collider.gameObject.transform.tag = "Untagged";
		}
	}
	public void enableHittableCollider() {
		foreach(Collider collider in colliderList) {
			collider.gameObject.transform.tag = "enemy_boss";
		}
	}
	public void change_hurtColor() {
		bodySkin.GetComponent<SkinnedMeshRenderer> ().material.SetColor ("_Color", hurtColor);
		tip_timer = 20f;
	}
	public void playAudio(int index) {
		audioController.playOnce (index);
	}

	public void spawnBuff() {
		buff_spawner.spawnBuff ();
	}

	void midEffects() {
		
	}

	void lowEffects() {
		
	}
	void dieEffects() {
		isDead = true;
		anim_3.SetTrigger ("A_die");
		anim_2.SetTrigger ("A_die");
		foreach (GameObject particle in dieParticles) {
			particle.GetComponent<ParticleSystem> ().Stop ();
			particle.GetComponent<ParticleSystem> ().Play ();
		}
		for(int n = 0; n < zombie_parent.Count; n++) {
			CapsuleCollider[] colliderList = zombie_parent [n].GetComponentsInChildren<CapsuleCollider> ();
			foreach (CapsuleCollider collider in colliderList) {
				collider.enabled = false;
			}
		}
		Time.timeScale = 0.2f;
		StartCoroutine (resumeTimeScale ());
	}
	public void gameOver() {
		levelManager.gameOver ();
	}
	IEnumerator resumeTimeScale() {
		yield return new WaitForSeconds (0.3f);
		Time.timeScale = 1f;
	}
}