﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Canvas_buff : MonoBehaviour {

	public Sprite[] spritList;
	public Image currentImage;
	public Image mainColor;
	public Image circleColor;
	public Slider slider;

	PlayerController player;

	void Start() {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
	}
	void Update() {
		slider.value = player.buff_timer / player.buff_duration;
	}

	public void setType(string type) {
		switch (type) {
		case "lighting":
			currentImage.sprite = spritList [0];
			mainColor.color = new Color (0f / 255f, 0f / 255f, 255f / 255f, 255f / 255f);
			circleColor.color = new Color (155f / 255f, 155f / 255f, 255f / 255f, 255f / 255f);
			break;
		case "fire":
			currentImage.sprite = spritList [1];
			mainColor.color = new Color (255f / 255f, 0f / 255f, 0f / 255f, 255f / 255f);
			circleColor.color = new Color (255f / 255f, 155f / 255f, 155f / 255f, 255f / 255f);
			break;
		case "wind":
			currentImage.sprite = spritList [2];
			mainColor.color = new Color (0f / 255f, 255f / 255f, 0f / 255f, 255f / 255f);
			circleColor.color = new Color (155f / 255f, 255f / 255f, 155f / 255f, 255f / 255f);
			break;
		}
	}
}
