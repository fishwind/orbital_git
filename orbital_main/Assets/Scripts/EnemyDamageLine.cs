﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public class EnemyDamageLine : MonoBehaviour {

	float damageAmount = 0f;
	float forceAmount = 0f;
	float throwUp = 0f;
	public bool is_big = true;
	bool hasHit = false;

	void OnTriggerEnter(Collider hit) {
		if (hasHit == false && hit.gameObject.tag == "Player") {
			hasHit = true;
			// dmg player by damageAmount;
			if (is_big) {
				hit.gameObject.GetComponent<PlayerController> ().player_hurt_big ();
				hit.gameObject.GetComponent<Rigidbody> ().AddExplosionForce (forceAmount, this.transform.position, 0, throwUp);
			} else {
				hit.gameObject.GetComponent<PlayerController> ().player_hurt_small ();
			}
		}
	}

	public void startDmgLine(float dmgAmt, float forceAmt, float up, bool isBig, bool hitAlr) {
		GetComponent<BoxCollider> ().enabled = true;
		this.damageAmount = dmgAmt;
		this.forceAmount = forceAmt;
		this.throwUp = up;
		this.is_big = isBig;
		this.hasHit = hitAlr;
	}
	public void endDmgLine() {
		GetComponent<BoxCollider> ().enabled = false;
	}
}
