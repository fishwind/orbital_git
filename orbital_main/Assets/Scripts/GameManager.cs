﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static bool stats_missionSucceed;
	public static int stats_timeTaken;		// 1
	public static int stats_dmgTaken;		// 2
	public static int stats_manaUsed;		// 3
	public static int stats_enemiesKilled;	// 4
	public static int stats_itemCollected;	// 5
	public static int stats_numberButton;	// 6
	public static int stats_numberJump;		// 7
	public static int stats_numberAttack;	// 8
	public static int stats_numberDash;		// 9
	public static int stats_numberHurt;		// 10
	public static int totalScore;
	public static string rank;

	public static int chosen_level;
	public static int chosen_character;
	public static GameObject[] character_list;
	public GameObject[] char_lst;

	static float timeTakenScale = 7f;
	static float dmgTakenScale = 3f;
	static float numberDashScale = 7f;
	static float enemiesKilledScale = 50f; 

	void Awake() {
		DontDestroyOnLoad (transform.gameObject);
		//PlayerPrefs.DeleteAll ();
		character_list = char_lst;
	}
	public static void spawnEverything() {
		Instantiate (character_list [chosen_character], new Vector3 (0, 0, 0), Quaternion.identity);
	}

	public static void calculateResult() {
		if(stats_missionSucceed) {
			totalScore = (int)(12000 - (stats_timeTaken * timeTakenScale) - (stats_dmgTaken * dmgTakenScale) + (stats_numberDash * numberDashScale) + (stats_enemiesKilled * enemiesKilledScale));
			if (totalScore >= 10000) {
				rank = "SS";
			} else if (totalScore >= 9000) {
				rank = "S"; 
			} else if (totalScore >= 8000) {
				rank = "A";
			} else if (totalScore >= 6500) {
				rank = "B";
			} else if (totalScore >= 5000) {
				rank = "C";
			} else if (totalScore >= 4500) {
				rank = "D";
			} else {
				rank = "E";
			}
			PlayerPrefs.SetInt ("highscore_6", totalScore);
		} else {
			totalScore = (int)(2800 - (stats_dmgTaken * dmgTakenScale) + (stats_numberDash * numberDashScale) + (stats_timeTaken * timeTakenScale) + (stats_enemiesKilled * enemiesKilledScale));
			if (totalScore >= 3000) {
				rank = "F";
			} else {
				rank = "GG";
			}
			PlayerPrefs.SetInt ("highscore_6", totalScore);
		}
	}
}
