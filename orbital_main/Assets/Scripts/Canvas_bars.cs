﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Canvas_bars : MonoBehaviour {

	public PlayerController player;
	public Slider slider_hp;
	public Slider slider_mana;


	void Update() {
		slider_hp.value = (player.currentHealth / player.maxHealth);
		slider_mana.value = (player.currentMana / player.maxMana);
	}
}	
