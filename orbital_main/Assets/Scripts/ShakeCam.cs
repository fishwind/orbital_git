﻿using UnityEngine;
using System.Collections;

public class ShakeCam : MonoBehaviour {

	public bool touch_trigger = false;

	Shaker shaker;
	float small_distance = 40f;
	float big_distance = 10f;
	bool touch_alr = false;

	void Start() {
		shaker = GameObject.FindGameObjectWithTag ("LookAtMe").GetComponent<Shaker> ();
	}
	public void shake_normal() {
		float distance = Vector3.Distance (this.transform.position, shaker.gameObject.transform.position);
		if (distance <= big_distance) {
			shaker.shake_big ();
		} else if(distance <= small_distance) {
			shaker.shake_small ();
		}
	}
	public void shake_small() {
		shaker.shake_small ();
	}
	public void shake_big() {
		shaker.shake_big ();
	}

	void OnCollisionEnter(Collision hit) {
		if (touch_trigger && !touch_alr) {
			shake_normal ();
			touch_alr = true;				
		}
	}
	void OnTriggerEnter(Collider hit) {
		if (touch_trigger && !touch_alr) {
			shake_normal ();
			touch_alr = true;				
		}
	}
}
