﻿using UnityEngine;
using System.Collections;

public class Bullet_missile : MonoBehaviour {

	public float missileSpeed = 20f;
	public float rotationSpeed = 0.5f;
	public GameObject selfExplosion_prefab;

	public GameObject[] dropping_items;
	public float dropping_chance = 0.1f;
	public float dropping_ultraChance = 0.6f;

	GameObject player;
	bool dropAlr = false;

	void Awake() {
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update() {
		Vector3 vecDiff = player.transform.position - this.transform.position;
		transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (vecDiff), rotationSpeed);
		transform.Translate(transform.InverseTransformDirection(transform.forward) * missileSpeed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider hit) {
		if (hit.gameObject.tag != "direction") {
			Instantiate (selfExplosion_prefab, transform.position, Quaternion.identity);
			Destroy (this.gameObject);
			float randomValue = Random.Range (0f, 1f);
			if (dropAlr == false && randomValue <= dropping_chance) {
				dropItem ();
				dropAlr = true;
			}
		}
	}
	void dropItem() {
		float randomValue = Random.Range (0f, 1f);
		Vector3 spawnpoint = transform.position;
		spawnpoint.y += 5f;
		if(randomValue <= dropping_ultraChance) {
			GameObject item = (GameObject)(Instantiate (dropping_items [0], spawnpoint, Quaternion.identity));
			item.GetComponent<Rigidbody> ().AddExplosionForce (200f, this.transform.position, 0f, 5f);
		} else {
			GameObject item = (GameObject)(Instantiate (dropping_items [Random.Range(1, dropping_items.Length)], spawnpoint, Quaternion.identity));
			item.GetComponent<Rigidbody> ().AddExplosionForce (200f, this.transform.position, 0f, 5f);
		}
	}
}
