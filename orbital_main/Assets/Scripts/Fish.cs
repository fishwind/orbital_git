﻿using UnityEngine;
using System.Collections;

public class Fish : MonoBehaviour {

	public EnemyHealth_manager health;
	public GameObject[] target_list;
	public Material[] material_list;
	public GameObject body;
	public float min_scale = 0.6f;
	public float max_scale = 1.2f;
	public float min_speed = 1f;
	public float max_speed = 5f;
	public float min_turnSpeed = 2f;
	public float max_turnSpeed = 8f;

	GameObject target;
	AudioController audioController;
	float move_speed;
	float turn_speed = 3f;
	bool isDead = false;
	void Awake() {
		health.enemyType = 1;
	}
	void Start() {
		audioController = GetComponent<AudioController> ();
		target = target_list[Random.Range(0, target_list.Length)];
		move_speed = Random.Range (min_speed, max_speed);
		transform.localScale = transform.localScale * Random.Range (min_scale, max_scale);
		body.GetComponent<SkinnedMeshRenderer> ().material = material_list [Random.Range (0, material_list.Length)];
	}

	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			} else {
				Vector3 vecDiff = target.transform.position - this.transform.position;
				transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (vecDiff), turn_speed * Time.deltaTime);
				transform.Translate (transform.InverseTransformDirection(transform.forward) * move_speed * Time.deltaTime);
				if (Vector3.Distance (this.transform.position, target.transform.position) <= 1f) {
					changeLocation ();
				}
			}
		}
	}

	void changeLocation() {
		target = target_list [Random.Range (0, target_list.Length)];
		move_speed = Random.Range (min_speed, max_speed);
		turn_speed = Random.Range (min_turnSpeed, max_turnSpeed);
	}
	void die() {
		GetComponent<Animator> ().enabled = false;
		GameManager.stats_enemiesKilled += 1;
		GetComponent<Rigidbody> ().isKinematic = false;
		GetComponent<Rigidbody> ().useGravity = true;
		GetComponent<BoxCollider> ().enabled = false;
		audioController.playOnce (0);
		transform.Find ("buff_spawner").gameObject.GetComponent<Buff_spawner_golem> ().spawnBuff ();
	}
}
