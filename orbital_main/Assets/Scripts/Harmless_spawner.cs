﻿using UnityEngine;
using System.Collections;

public class Harmless_spawner : MonoBehaviour {

	public GameObject butterfly_prefab;
	int duplicate;

	void Start () {
		duplicate = Random.Range (4, 6);
		for (int n = 0; n < duplicate; n++) {
			Instantiate (butterfly_prefab, this.transform.position, Quaternion.identity);
		}
	}
}
