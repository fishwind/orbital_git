﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Canvas_iconBullet : MonoBehaviour {

	public RectTransform[] iconList;	// a list here so next time can edit their picture
	public RectTransform panel;
	public RectTransform frame;
	public Text ultra_bullet_text;
	public float enlarge_scale = 1.5f;
	public float scaleSpeed = 0.5f;
	public float frameTranslateSpeed = 0.5f;
	public float enlarge_cooldown = 1;


	float targetFramePosition_x = -17;
	float enlarge_time = 0;
	Vector3 originalScale;

	void Start () {
		originalScale = panel.localScale;
	}

	void Update() {
		if (enlarge_time > Time.time) {
			Vector3 targetScale = new Vector3 (originalScale.x * enlarge_scale, originalScale.y * enlarge_scale, originalScale.z * enlarge_scale);
			panel.localScale = Vector3.Lerp (panel.localScale, targetScale, Time.deltaTime * scaleSpeed);
		} else {
			panel.localScale = Vector3.Lerp (panel.localScale, originalScale, Time.deltaTime * scaleSpeed);
		}
		frame.localPosition = Vector3.Lerp (frame.localPosition, new Vector3(targetFramePosition_x,frame.localPosition.y, frame.localPosition.z) , Time.deltaTime * frameTranslateSpeed);
	}

	public void changeIcon(int index) {
		targetFramePosition_x = -17 + (index * 146.67f);
		enlarge_time = Time.time + enlarge_cooldown;
	}
	public void update_ultraBullet(int value) {
		ultra_bullet_text.text = value.ToString();
	}
}
