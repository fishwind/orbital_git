﻿using UnityEngine;
using System.Collections;

public class Boss_golem2 : MonoBehaviour {

	public GameObject boss_golem1;

	void boss_teleport() {
		boss_golem1.GetComponent<Boss_golem> ().teleport ();
	}
	void boss_spin() {
		boss_golem1.GetComponent<Boss_golem> ().anim_3.SetTrigger ("A_spin");
	}
	void boss_disableSolidCollider() {
		boss_golem1.GetComponent<Boss_golem> ().disableSolidCollider ();
	}
	void boss_enableSolidCollider() {
		boss_golem1.GetComponent<Boss_golem> ().enableSolidCollider ();
	}
	void boss_start_hand_energy(){
		boss_golem1.GetComponent<Boss_golem> ().start_hand_energy ();
	}
	void boss_end_hand_energy(){
		boss_golem1.GetComponent<Boss_golem> ().end_hand_energy ();
	}
	void boss_disableHittableCollider() {
		boss_golem1.GetComponent<Boss_golem> ().disableHittableCollider ();
	}
	void boss_enableHittableCollider() {
		boss_golem1.GetComponent<Boss_golem> ().disableHittableCollider ();
	}
	void boss_playAudio(int index) {
		boss_golem1.GetComponent<Boss_golem> ().playAudio (index);
	}
	void boss_gameOver(){
		boss_golem1.GetComponent<Boss_golem> ().gameOver ();
	}
	void boss_spawnBuff() {
		boss_golem1.GetComponent<Boss_golem> ().spawnBuff ();
	}
}
