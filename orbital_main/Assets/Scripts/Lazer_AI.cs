﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lazer_AI : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageLine dmgLine;
	public LineRenderer lazerLineRender;
	public GameObject enemyExplosion;
	public LookAtPlayer lookAtPlayer;
	public float close_enough_distance = 40f;

	public float shootDelay = 5f;
	public float dmgLine_amount = 200f;
	public float dmgLine_force = 0f;
	public float dmgLine_throwUp = 0f;
	public bool dmgLine_isBig = true;	// big sphere trigger player's hurtBig animation


	string[] shootAnim = { "A_shoot", "A_shoot2" };
	bool isShooting = false;
	bool isDead = false;
	GameObject player;
	Animator anim;
	AudioController audioController;

	void Awake() {
		health.enemyType = 2;
	}

	void Start() {
		InvokeRepeating ("startAnim", 5f, shootDelay);
		player = GameObject.FindGameObjectWithTag ("Player");
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
	}
	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				dead ();
				isDead = true;
			} else {
				if (isShooting) {
					//dmgLine.startDmgLine (dmgLine_amount, dmgLine_force, dmgLine_throwUp, dmgLine_isBig, false);
				} else {
					//dmgLine.endDmgLine ();
				}
			}
		}
	}

	void startAnim() {
		if (Vector3.Distance (this.transform.position, player.transform.position) <= close_enough_distance) {
			anim.SetTrigger (shootAnim[Random.Range(0, shootAnim.Length)]);
		}
	}
	void startLazer() {
		isShooting = true;
		lazerLineRender.enabled = true;
		lookAtPlayer.enabled = false;
		dmgLine.startDmgLine (dmgLine_amount, dmgLine_force, dmgLine_throwUp, dmgLine_isBig, false);
		audioController.playOnce (1);
	}
	void endLazer() {
		lazerLineRender.enabled = false;
		isShooting = false;
		anim.SetTrigger ("A_idle");
		lookAtPlayer.enabled = true;
		dmgLine.endDmgLine ();
	}
	void dead() {
		endLazer ();
		audioController.playOnce (0);
		GameManager.stats_enemiesKilled += 1;
		Instantiate (enemyExplosion, transform.position, Quaternion.identity);
		GetComponent<BoxCollider> ().enabled = false;
		GetComponent<Rigidbody> ().isKinematic = false;
		Destroy (this.gameObject, 3f);
	}

}