﻿using UnityEngine;
using System.Collections;

public class Whale : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageLine dmgLine;
	public float chase_distance = 150f;
	public float dmgLine_amount = 200f;
	public float dmgLine_force = 2500;
	public float dmgLine_forceUp = 1f;
	public bool dmgLine_isBig = true;

	float move_speed = 15f;
	float turn_speed = 3f;
	GameObject player;
	AudioController audioController;
	bool isDead = false;
	bool isGround = false;
	bool isAir = false;

	void Awake() {
		health.enemyType = 4;
	}
	void Start() {
		audioController = GetComponent<AudioController> ();
		Animation anim = GetComponent<Animation> ();
		anim ["fastswim"].speed = 3f;
		player = GameObject.FindGameObjectWithTag ("Player");
		InvokeRepeating ("reset_dmgSphere", 1f, 3f);
	}

	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			} else if (isGround && isAir) {
				hurt ();
			} else {
				if (Vector3.Distance (this.transform.position, player.transform.position) <= chase_distance) {
					chase ();
				}
			}
		}
	}

	void OnTriggerStay(Collider hit) {
		if (hit.gameObject.tag == "Water") {
			GetComponent<Rigidbody> ().useGravity = false;
			isAir = false;
		}
		if (hit.gameObject.tag == "ground") {
			isGround = true;
		} 
	}
	void OnTriggerExit(Collider hit) {
		if (hit.gameObject.tag == "Water") {
			isAir = true;
			GetComponent<Rigidbody> ().useGravity = true;
		}
	}
	void hurt() {
		health.currentHealth -= 3f;
	}
	void chase() {
		Vector3 vecDiff = player.transform.position - this.transform.position;
		transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.LookRotation (vecDiff), turn_speed * Time.deltaTime);
		transform.Translate (transform.InverseTransformDirection(transform.forward) * move_speed * Time.deltaTime);
	}
	void die() {
		GameManager.stats_enemiesKilled += 1;
		GetComponent<Rigidbody> ().isKinematic = false;
		GetComponent<Rigidbody> ().drag = 0f;
		GetComponent<Rigidbody>().useGravity = true;
		BoxCollider[] colliders = GetComponents<BoxCollider> ();
		audioController.playOnce (0);
		foreach (BoxCollider col in colliders) {
			col.enabled = false;
		}
		Destroy (this.gameObject, 10f);
	}
	void reset_dmgSphere() {
		dmgLine.endDmgLine ();
		dmgLine.startDmgLine (dmgLine_amount, dmgLine_force, dmgLine_forceUp, dmgLine_isBig, false);
	}
}
