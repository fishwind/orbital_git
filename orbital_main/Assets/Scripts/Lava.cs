﻿using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {

	Vector3 targetScale;
	bool hasHit = false;
	float hurtTimer = 0f;
	float hurtDelay = 0.3f;

	void Update() {
		if (Time.time > hurtTimer) {
			hurtTimer = Time.time + hurtDelay;
			hasHit = false;
		}
	}
	void OnTriggerStay(Collider hit) {
		if (hasHit == false && hit.gameObject.tag == "Player") {
			hasHit = true;
			hit.gameObject.GetComponent<PlayerController> ().player_hurt_small ();
		}
	}
}
