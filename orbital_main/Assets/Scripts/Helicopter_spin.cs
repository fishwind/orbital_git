﻿using UnityEngine;
using System.Collections;

public class Helicopter_spin : MonoBehaviour {

	public float speed = 150f;
	
	void FixedUpdate () {
		transform.Rotate (Vector3.forward, Time.deltaTime * speed);
	}
}
