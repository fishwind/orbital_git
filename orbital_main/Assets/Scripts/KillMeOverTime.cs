﻿using UnityEngine;
using System.Collections;

public class KillMeOverTime : MonoBehaviour {

	public float lifeTime = 5;

	void Awake () {
		Destroy (gameObject, lifeTime);
	}

}
