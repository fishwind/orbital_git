﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// only boss don't need slider canvas
public class EnemyHealth_manager : MonoBehaviour {

	public int enemyType = 1;
	public GameObject enemy;
	public float health_type1;
	public float health_type2;
	public float health_type3;
	public float health_type4;
	public float health_type5;

	public float maxHealth;
	public float currentHealth;

	public Slider slider;	// only for enemyType 1 ~ 4

	void Awake() {
		switch (enemyType) {
		case 1: 
			maxHealth = health_type1;
			currentHealth = maxHealth;
			break;
		case 2: 
			maxHealth = health_type2;
			currentHealth = maxHealth;
			break;
		case 3: 
			maxHealth = health_type3;
			currentHealth = maxHealth;
			break;
		case 4: 
			maxHealth = health_type4;
			currentHealth = maxHealth;
			break;
		case 5: 
			maxHealth = health_type5;
			currentHealth = maxHealth;
			break;
		}
	}

	void Update() {
		currentHealth = Mathf.Clamp (currentHealth, 0, maxHealth);
		if (enemyType != 5) {
			slider.value = (currentHealth / maxHealth);
		}
	}

	void OnCollisionEnter(Collision hit) {
		switch (enemyType) {
		case 1: 
			if (hit.gameObject.tag == "bullet1") {
				//takeDamage (10);
			}
			break;
		case 2: 
			if (hit.gameObject.tag == "bullet1") {
				
			}
			break;
		case 3: 
			if (hit.gameObject.tag == "bullet1") {
				
			}
			break;
		case 4: 
			if (hit.gameObject.tag == "bullet1") {
				
			}
			break;
		case 5: 
			if (hit.gameObject.tag == "bullet1" && currentHealth > 0) {
				//enemy.GetComponent<Boss_AI> ().hurtEffects();
			}
			break;
		}
	}
	public void takeDamage(float amount) {
		currentHealth -= amount;
	}

}
