﻿using UnityEngine;
using System.Collections;

public class Underwater_projection : MonoBehaviour {

	public float fps = 30.0f;
	public Texture2D[] frames;

	int frameIndex = 0;
	Projector projector;

	void Start() {
		projector = GetComponent<Projector> ();
		playFrame ();
		InvokeRepeating ("playFrame", 1 / fps, 1 / fps);
	}

	void playFrame() {
		projector.material.SetTexture ("_MainTex", frames [frameIndex]);
		frameIndex = (frameIndex + 1) % frames.Length;
	}
}
