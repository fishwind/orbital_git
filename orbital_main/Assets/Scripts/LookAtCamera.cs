﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {

	public float followDelay = 0.015f;
	public bool hasDelay = false;

	GameObject camera;

	void Start(){ 
		camera = GameObject.FindGameObjectWithTag ("MainCamera");
	}
	void FixedUpdate () {
		if (!hasDelay) {
			transform.LookAt (camera.transform.position);
		} else {
			Vector3 vecDiff = camera.transform.position - this.transform.position;
			this.transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.LookRotation(vecDiff), followDelay);
		}
	}
}
