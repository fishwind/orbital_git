﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bullet_ultra : MonoBehaviour {
	
	public float hitSmall_power = 550f;
	public bool is_beam = false;
	public List<GameObject> hitList;
	public float hit_dmg = 600;

	float lifeTime = 10f;

	void Start() {
		if (!is_beam) {
			Destroy (gameObject, lifeTime);
		}
	}

	void OnTriggerEnter(Collider hit) {
		if(is_beam) {
			if (!hitList.Contains (hit.gameObject)) {
				if (hit.gameObject.tag == "enemy_small") {
					hit.gameObject.GetComponent<Rigidbody>().AddExplosionForce(hitSmall_power, transform.position, 0, 0.5f);	// add force to hit object
					hit.gameObject.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				} else if (hit.gameObject.tag == "enemy_big") {
					hit.gameObject.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
				}
				hitList.Add (hit.gameObject);
			}
		}
	}
}
