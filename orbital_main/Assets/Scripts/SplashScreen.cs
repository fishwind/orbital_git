﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class SplashScreen : MonoBehaviour {
	
	AsyncOperation async;

	void startLoad() {
		async = SceneManager.LoadSceneAsync (3);
		async.allowSceneActivation = false;
	}
	void startGame() {
		async.allowSceneActivation = true;
	}

}
