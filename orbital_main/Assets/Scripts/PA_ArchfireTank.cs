using UnityEngine;
using System.Collections;

public class PA_ArchfireTank : MonoBehaviour {

	public EnemyHealth_manager health;
	public GameObject bulletPrefab;
	public GameObject particle_explodeSelf;

	public float chaseSpeed = 12f;
	public float scale_min = 0.5f;
	public float scale_max = 2f;
	public float height_min = 0;
	public float height_max = 0;
	public float stoppingDistance_min = 25;
	public float stoppingDistance_max = 35;
	public float close_enough_distance = 400;
	public float shootDelay_min = 3f;
	public float shootDelay_max = 6f;
	public float bullet_speed = 3000f;
	public float destroyDelay = 3f;
	public GameObject spawnpoint1;
	public GameObject spawnpoint2;

	GameObject player;
	GameObject body;	// to access spawnpoints easier
	float shootTimer = 3f;
	float stoppingDistance;
	bool close_enough = false;	// trigger if get close enough
	bool isDead = false;
	AudioController audioController;

	void Awake() {
		health.enemyType = 1;
	}
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		body = transform.Find ("body").gameObject;
		body.transform.localPosition = new Vector3 (body.transform.localPosition.x, Random.Range(height_min, height_max), body.transform.localPosition.z);
		transform.localScale = transform.localScale * Random.Range(scale_min, scale_max);
		stoppingDistance = Random.Range (stoppingDistance_min, stoppingDistance_max);
		audioController = GetComponent<AudioController> ();
	}
	void Update () {
		if (isDead == false) {
			if (health.currentHealth > 0 && close_enough && Time.time > shootTimer) {
				shoot ();
			}
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			}
		}
	}
	void FixedUpdate() {
		float distance = Vector3.Distance (transform.position, player.transform.position);
		if (distance > stoppingDistance) {
			chase ();
		}
		if (distance <= close_enough_distance) {
			close_enough = true;
		} else {
			close_enough = false;
		}
	}
	void shoot() {
		shootTimer = Time.time + Random.Range (shootDelay_min, shootDelay_max);
		Vector3 position1 = spawnpoint1.transform.position;
		Vector3 position2 = spawnpoint2.transform.position;
		Quaternion rotation1 = spawnpoint1.transform.rotation;	// spawn bullet will have rotation = body's rotation
		Quaternion rotation2 = spawnpoint2.transform.rotation;	// spawn bullet will have rotation = body's rotation
		GameObject bul1 = (GameObject)Instantiate(bulletPrefab, position1, rotation1);
		bul1.GetComponent<BeamParam>().SetBeamParam(this.GetComponent<BeamParam>());
		GameObject bul2 = (GameObject)Instantiate(bulletPrefab, position2, rotation2);
		bul2.GetComponent<BeamParam>().SetBeamParam(this.GetComponent<BeamParam>());
		audioController.playOnce (0);
	}
	void chase() {
		transform.Translate (Vector3.forward * chaseSpeed * Time.deltaTime);
	}
	void die() {
		GameManager.stats_enemiesKilled += 1;
		GetComponent<Rigidbody> ().isKinematic = false;
		BoxCollider[] colliders = GetComponentsInChildren<BoxCollider> ();
		foreach (BoxCollider col in colliders) {
			col.enabled = false;
		}
		GameObject explode = (GameObject)(Instantiate (particle_explodeSelf, body.transform.position, Quaternion.identity));
		explode.transform.parent = body.gameObject.transform;
		audioController.playOnce (1);
		Destroy (this.gameObject, destroyDelay);
	}
}