﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Boss_AI_missile : MonoBehaviour {

	public Boss_AI boss_AI;
	public GameObject[] spawnpoint;
	public GameObject missile_prefab;
	public GameObject[] shootEffect;
	public float shootDelay_high = 15f;
	public float shootDelay_mid = 10f;
	public float shootDelay_low = 8f;
	public float missilePower = 1000f;

	Animator anim;
	float shootDelay = 10f;
	float shootTimer = 0f;
	string[] stringList = {"A_shootH", "A_shootM", "A_shootL" };

	void Start() {
		anim = GetComponent<Animator> ();
		shootDelay = shootDelay_high;
		shootTimer = Time.time + shootDelay;
	}

	void Update() {
		switch (boss_AI.getHealthLevel ()) {
		case "high":
			shootDelay = shootDelay_high;
			break;
		case "mid":
			shootDelay = shootDelay_mid;
			break;
		case "low":
			shootDelay = shootDelay_low;
			break;
		}
		if (!boss_AI.isDead && !boss_AI.isShootingMissile && Time.time > shootTimer) {
			shootTimer = Time.time + shootDelay;
			boss_AI.isShootingMissile = true;
			if (boss_AI.getHealthLevel () == "high") {
				anim.SetTrigger (stringList [0]);
			} else if (boss_AI.getHealthLevel () == "mid") {
				anim.SetTrigger (stringList [1]);
			} else if (boss_AI.getHealthLevel () == "low") {
				anim.SetTrigger (stringList [2]);
			} 
		}
	}
	void shootR() {
		shootEffect[0].GetComponent<ParticleSystem> ().Stop ();
		shootEffect[0].GetComponent<ParticleSystem> ().Play ();
		GameObject bul = (GameObject)(Instantiate (missile_prefab, spawnpoint [0].transform.position, spawnpoint [0].transform.rotation));
		bul.GetComponent<Rigidbody> ().isKinematic = false;
		bul.GetComponent<Rigidbody> ().AddRelativeForce (spawnpoint [0].transform.InverseTransformDirection(spawnpoint [0].transform.forward) * missilePower);
		bul.transform.localScale = bul.transform.localScale * 1.12f;
		bul.GetComponent<Bullet_missile> ().missileSpeed = 20;
	}
	void shootL() {
		shootEffect[1].GetComponent<ParticleSystem> ().Stop ();
		shootEffect[1].GetComponent<ParticleSystem> ().Play ();
		GameObject bul = (GameObject)(Instantiate (missile_prefab, spawnpoint [1].transform.position, spawnpoint [0].transform.rotation));
		bul.GetComponent<Rigidbody> ().isKinematic = false;
		bul.GetComponent<Rigidbody> ().AddRelativeForce (spawnpoint [0].transform.InverseTransformDirection(spawnpoint [0].transform.forward) * missilePower);
		bul.transform.localScale = bul.transform.localScale * 1.12f;
		bul.GetComponent<Bullet_missile> ().missileSpeed = 20;
	}
	public void endMissile() {
		boss_AI.isShootingMissile = false;
		anim.SetTrigger ("A_idle");
	}

}
