﻿using UnityEngine;
using System.Collections;

public class Bullet_1 : MonoBehaviour {

	public ParticleSystem explosion;
	public ParticleSystem ownParticle;
	public float hit_dmg = 10f;
	AudioController audioController;
	bool exploded = false;
	Vector3 hitPosition;
	MeshRenderer render;
	bool hasHitPlayer;

	void Start() {
		audioController = GetComponent<AudioController> ();
		render = GetComponent<MeshRenderer> ();
		hasHitPlayer = false;
	}
	void Update() {
		if (exploded) transform.position = hitPosition;
	}
	void OnCollisionEnter(Collision hit) {
		if (!exploded) {
			exploded = true;
			render.enabled = false;
			hitPosition = transform.position;
			audioController.playOnce (0);
			explosion.Stop ();
			explosion.Play ();
			//Instantiate (explosion, transform.position, Quaternion.identity);
			ownParticle.enableEmission = false;
			Destroy (this.gameObject, 1f);
			if (hit.gameObject.tag == "enemy_small" || hit.gameObject.tag == "enemy_big") {
				hit.gameObject.GetComponent<EnemyHealth_manager> ().takeDamage (hit_dmg);
			}
			GetComponent<SphereCollider>().enabled = false;
		}
		if (hit.gameObject.tag == "Player" && !hasHitPlayer) {
			hasHitPlayer = true;
			hit.gameObject.GetComponent<PlayerController> ().player_hurt_big ();
		}
	}
}
