﻿using UnityEngine;
using System.Collections;

public class Buff_spawner_golem : MonoBehaviour {

	public Transform[] spawnpoint;
	public GameObject[] buff_prefab;

	float chance = 0.1f;

	public void spawnBuff() {
		foreach(Transform point in spawnpoint) {
			float value = Random.Range(0f, 1f);
			if(value <= chance) {
				GameObject buff = (GameObject)(Instantiate(buff_prefab[Random.Range(0, buff_prefab.Length)], point.position, Quaternion.identity));
				buff.GetComponent<Rigidbody>().AddExplosionForce(900f, this.transform.position, 30f, 15f);
			}
		}
	}

}
