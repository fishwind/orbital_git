﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

public class MenuController : MonoBehaviour {

	public Menu currentMenu;
	public GameObject playExit_panel;
	public GameObject levelSelect_panel;
	public GameObject jumpParticles;
	public GameObject characteSelection_prefab;
	public Texture2D mouseImage;
	public GameObject loadingScreen;
	public Slider loadingBar;
	public Image loadingImage;
	public Text loadingText;
	public Text[] highscore_text;
	public Sprite keyboard1;
	public Sprite keyboard2;
	public Sprite keyboard0;

	AudioController audioController;
	int chosen_level = 1;		// start from 1
	//int chosen_character = 0;	// start from 0
	float fakeIncrement;
	float fakeTiming;
	Animator cameraAnim;
	Animator playerAnim;
	AsyncOperation async;

	public void Start() {
		Cursor.SetCursor(mouseImage, new Vector2(0,0), CursorMode.ForceSoftware);
		cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();
		playerAnim = GameObject.FindGameObjectWithTag ("Player").GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		showMenu(currentMenu);
		StartCoroutine (spinCharacter());
		update_highscores ();
	}

	void update_highscores() {	// store all 6 playerprefs to array---> sort array ----> replace PlayerPref with top 5 scores
		highscore_text [0].text = PlayerPrefs.GetInt ("highscore_1").ToString();
		highscore_text [1].text = PlayerPrefs.GetInt ("highscore_2").ToString();
		highscore_text [2].text = PlayerPrefs.GetInt ("highscore_3").ToString();
		highscore_text [3].text = PlayerPrefs.GetInt ("highscore_4").ToString();
		highscore_text [4].text = PlayerPrefs.GetInt ("highscore_5").ToString();
	}
	public void showMenu(Menu menu) {
		if (currentMenu != null) currentMenu.IsOpen = false;
		currentMenu = menu;
		currentMenu.IsOpen = true;
	}
	public void playClickSound() {
		audioController.playOnce (2);
	}
	public void open_playExit() {
		playExit_panel.SetActive (true);
		levelSelect_panel.SetActive (false);
	}
	public void open_levelSelect() {
		playExit_panel.SetActive (false);
		levelSelect_panel.SetActive (true);
	}
	public void set_levelChosen(int level) {
		chosen_level = level;
		playerAnim.SetTrigger ("A_duck");
		StartCoroutine (jumpCharacter ());
	}
	public void blurCamera(){
		cameraAnim.gameObject.GetComponent<DepthOfField> ().enabled = true;
	}
	public void unblurCamera() {
		cameraAnim.gameObject.GetComponent<DepthOfField> ().enabled = false;
	}
	public void play_cameraRotation() {
		cameraAnim.SetTrigger ("A_cameraRotate");
		StartCoroutine (spawn_characterSelection ());
	}
	//public void set_characterChosen(int type) {
	//	chosen_character = type;
	//}
	public void run_levelChosen() {
		GameManager.chosen_level = this.chosen_level;
		GameManager.chosen_character = characteSelection_prefab.GetComponent<CharacterSelection_controller> ().index;
		StartCoroutine (run_levelChosen_delay ());
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------*/
	IEnumerator run_levelChosen_delay() {
		audioController.playOnce(4);
		yield return new WaitForSeconds(3.5f);
		StartCoroutine (loadLevelWithFakeRealBar ());
	}

	IEnumerator spinCharacter() {
		yield return new WaitForSeconds(0.85f);
		playerAnim.SetTrigger ("A_spin");
		audioController.playOnce (0);
	}
	IEnumerator jumpCharacter() {
		yield return new WaitForSeconds (0.75f);
		playerAnim.SetTrigger ("A_jump");
		Instantiate (jumpParticles, playerAnim.gameObject.transform.position, Quaternion.identity);
		playerAnim.gameObject.GetComponent<Rigidbody> ().AddRelativeForce (Vector3.up * 500f);
		audioController.playOnce (1);
	}
	IEnumerator spawn_characterSelection() {
		yield return new WaitForSeconds (2.75f);
		characteSelection_prefab.SetActive(true);
		yield return new WaitForSeconds (0.5f);
		audioController.playOnce(3); 	
	}
		
	public IEnumerator loadLevelWithFakeRealBar() {
		loadingScreen.SetActive (true);
		if (GameManager.chosen_level == 0) {
			loadingImage.sprite = keyboard0;
		} else if (GameManager.chosen_level == 1) {
			loadingImage.sprite = keyboard1;
		} else if (GameManager.chosen_level == 2) {
			loadingImage.sprite = keyboard2;
		}

		yield return new WaitForSeconds (1);
		async = SceneManager.LoadSceneAsync (GameManager.chosen_level);
		async.allowSceneActivation = false;
		while (loadingBar.value != 1f) {
			if(async.progress == 0.9f || loadingBar.value <= async.progress){
				fakeIncrement = UnityEngine.Random.Range (0.01f, 0.1f);
				fakeTiming = UnityEngine.Random.Range (0.01f, 0.5f);
				loadingBar.value += fakeIncrement;
			}
			yield return new WaitForSeconds (fakeTiming);
		}
		while(loadingBar.value == 1f) {
			if (GameManager.chosen_level != 0) {
				loadingText.text = "Press 'SPACE' to continue";
				if (Input.GetKeyDown ("space")) {
					loadingText.text = "Starting...";
					async.allowSceneActivation = true;
				}
			} else {
				loadingText.text = "Starting...";
				async.allowSceneActivation = true;
			}
			yield return null;
		}
	}
	/*--------------------------------------------------------------------------------------------------------------------------------------------*/

	public void exitGame() {
		Application.Quit ();
	}


}
