﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Canvas_screenEffect : MonoBehaviour {
	
	public Image hurtPic;
	public Image glassWaterPic;
	public float hurtAlpha = 185f;
	public float hurtDelay = 0.7f;
	public bool justExitWater;

	Color hurt_startColor;	// max alpha color
	Color hurt_endColor;	// 0 alpha color
	float glassWater_startOffset = 0.8f;
	float glassWater_targetOffset = 1f;
	float glassWater_startDistort = 80f;
	float glassWater_duration = 1.5f;
	Animator anim;

	void Start() {
		GetComponent<Canvas> ().worldCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
		anim = GetComponent<Animator>();
		Color tempColor1 = hurtPic.color;
		Color tempColor2 = hurtPic.color;
		tempColor1.a = hurtAlpha;
		hurt_startColor = tempColor1;
		tempColor2.a = 0f;
		hurt_endColor = tempColor2;
		justExitWater = false;
		glassWaterPic.enabled = false;
	}

	void Update() {
		hurtPic.color = Color.Lerp (hurtPic.color, hurt_endColor, hurtDelay * Time.deltaTime);
		if (justExitWater) {
			//Vector2 targetOffset = new Vector2 (0f, glassWater_startOffset);
			//Vector2 initialOffset = new Vector2 (0f, 0.7f);
			Vector2 targetOffset = new Vector2 (0f, glassWater_targetOffset);
			glassWaterPic.material.SetFloat ("_BumpAmt", Mathf.Lerp (glassWaterPic.material.GetFloat ("_BumpAmt"), 0f, glassWater_duration * Time.deltaTime));
			glassWaterPic.material.SetTextureOffset("_BumpMap", Vector2.Lerp (glassWaterPic.material.GetTextureOffset("_BumpMap"), targetOffset, glassWater_duration * Time.deltaTime));
			if (glassWaterPic.material.GetFloat ("_BumpAmt") <= 1) {
				justExitWater = false;
				glassWaterPic.enabled = false;
				//glassWaterPic.material.SetFloat ("_BumpAmt", glassWater_startDistort);
				//glassWaterPic.material.SetTextureOffset ("_BumpMap", initialOffset);
			}
		}
	}

	public void trigger_hurtEffect() {
		hurtPic.color = hurt_startColor;
	}
	public void trigger_justExitWater() {
		justExitWater = true;
		glassWaterPic.enabled = true;
		Vector2 initialOffset = new Vector2 (0f, glassWater_startOffset);
		glassWaterPic.material.SetFloat ("_BumpAmt", glassWater_startDistort);
		glassWaterPic.material.SetTextureOffset ("_BumpMap", initialOffset);
	}
	public void fadeBlack() {
		anim.SetTrigger ("A_die");
	}

}
