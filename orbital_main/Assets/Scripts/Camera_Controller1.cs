﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Water;

[RequireComponent(typeof (Animator))]
public class Camera_Controller1 : MonoBehaviour {
	
	public GameObject unityChan_handR;
	public GameObject unityChan_meshR;
	public GameObject unityChan_handL;
	public GameObject unityChan_candyhandR;
	public GameObject unityChan_candymeshR;
	public GameObject unityChan_candyhandL;
	public GameObject unityChan_sodeR;
	public GameObject unityChan_sodeL;
	public GameObject unityChan_sodecandyR;
	public GameObject unityChan_sodecandyL;
	public GameObject water;
	public GameObject water_back;
	public GameObject water_front;
	public GameObject directional_arrow;
	public GameObject boss;
	public GameObject bossPackage;
	public Material boss_sky;
	public ParticleSystem[] environment_particles;
	public float damp = 7f;
	public float defaultFOW = 60;
	public float rotationSpeedX = 1f;
	public float vortex_duration = 1.15f;
	public bool is_underwater = false;

	Vector3 offset;
	Animator anim;
	Animator direction_anim;
	AudioController audioController;
	Canvas_screenEffect glassWater;
	ParticleSystem lastParticle;
	GameObject unityChan_hand;
	GameObject player;
	GameObject target;
	Color color_initial = new Color (45f / 255f, 45f / 255f, 45f / 255f);
	Color color_forest = new Color(75f/255f, 0f/255f, 95f/255f);
	Color color_snow = new Color(99f/255f , 113f/255f, 255f/255f);
	Color color_rain = new Color(1f,1f,1f);
	Color color_preBoss = new Color (10f/255f, 124f/255f, 91f/255f);
	Color color_preBoss_skybox = new Color (0f/255f, 255f/255f, 0f/255f);
	Color color_boss = new Color(188f/255f , 70f/255f, 70f/255f);
	Color color_boss_skybox = new Color (32f/255f, 114f/255f, 0f/255f);
	Color color_bossFog = new Color (227f/255f, 125f/255f, 0f/255f);
	Color color_metor = new Color (255/255f, 136/255f, 0/255f);
	Color color_metor_skyBox = new Color (116 / 255f, 78 / 255f, 38 / 255f);
	Color color_rock = new Color (255f/255f, 175f/255f, 0f/255f);
	int environment_index;
	float initialExposure = 0.85f;
	float minX = -89f;			// X means rotate in X-axis
	float maxX = 89;			// thus, look up and down
	float rotateX = 0f;
	float color_duration = 0.5f;
	float color_duration_preBoss = 2f;
	float currentExposure;
	Color currentColor;
	bool is_aimming = false;
	bool is_dead = false;


	SunShafts sunShaft;
	EdgeDetection edgeDetect;
	Fisheye fisheye;
	Vortex vortex;	
	GlobalFog fog;
	//VignetteAndChromaticAberration vignette;
	Projector projector;
	float edgeDetection_speed = 5f;
	float vortex_start = -0.75f;
	float vortex_end = 1.75f;

	void Awake() {
		if (GameManager.chosen_level == 2) {
			if (GameManager.chosen_character == 7) {
				unityChan_hand = unityChan_candyhandR;
				unityChan_meshR.SetActive (false);
				unityChan_candymeshR.SetActive (true);
				unityChan_handL.SetActive (false);
				unityChan_candyhandL.SetActive (true);
			} else {
				unityChan_hand = unityChan_handR;
				unityChan_meshR.SetActive (true);
				unityChan_candymeshR.SetActive (false);
				unityChan_handL.SetActive (true);
				unityChan_candyhandL.SetActive (false);
			}
		} else {
			unityChan_hand = unityChan_handR;
		}
	}

	void Start () {
		if (GameManager.chosen_level == 2 && GameManager.chosen_character == 7) {
			unityChan_hand = unityChan_candyhandR;
		}
		player = GameObject.FindGameObjectWithTag ("Player");
		target = GameObject.FindGameObjectWithTag ("LookAtMe");
		glassWater = GameObject.Find ("Canvas_screenEffects").GetComponent<Canvas_screenEffect>();
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		offset = target.transform.position - transform.position;
		projector = GameObject.FindGameObjectWithTag ("Projector").GetComponent<Projector> ();
		projector.enabled = false;
		vortex = GetComponent<Vortex> ();
		fisheye = GetComponent<Fisheye> ();
		edgeDetect = GetComponent<EdgeDetection> ();
		sunShaft = GetComponent<SunShafts> ();
		fog = GetComponent<GlobalFog> ();
		//vignette = GetComponent<VignetteAndChromaticAberration>();
		is_underwater = false;
		vortex.enabled = false;
		fisheye.enabled = false;
		edgeDetect.enabled = false;
		sunShaft.enabled = false;
		direction_anim = directional_arrow.GetComponent<Animator> ();
		if (GameManager.chosen_level == 1) {
			environment_index = 1;
		} else if (GameManager.chosen_level == 2) {
			environment_index = 2;
			currentExposure = initialExposure;
			currentColor = color_initial;
			RenderSettings.skybox.SetColor ("_Tint", currentColor);
			RenderSettings.skybox.SetFloat ("_Exposure", initialExposure);
		}
	}
	void Update() {
		if (is_underwater) {
			playVortex ();
		}
		if (is_dead) {
			edgeDetect.enabled = true;
			edgeDetect.edgesOnlyBgColor = new Color (1,1,1);
			edgeDetect.lumThreshold = Mathf.Lerp (edgeDetect.lumThreshold, 0.01f, Time.deltaTime * edgeDetection_speed);
			edgeDetect.sampleDist = Mathf.Lerp (edgeDetect.sampleDist, 0.32f, Time.deltaTime * edgeDetection_speed);
			edgeDetect.edgesOnly = Mathf.Lerp (edgeDetect.edgesOnly, 1, Time.deltaTime * edgeDetection_speed);
		}
	}
	void FixedUpdate () {
		if (!is_aimming) {
			float desiredAngle = target.transform.eulerAngles.y;
			Quaternion rotation = Quaternion.Euler (0, desiredAngle, 0);
			Vector3 desiredPosition = (target.transform.position - (rotation * offset));
			transform.position = Vector3.Lerp (transform.position, desiredPosition, damp * Time.deltaTime);
			transform.LookAt (target.transform.position);
			unityChan_hand.SetActive (false);
			rotateX = 0;
		} else {
			rotateX += Input.GetAxis ("Mouse Y") * rotationSpeedX;
			rotateX = Mathf.Clamp (rotateX, minX, maxX);
			transform.localEulerAngles = new Vector3 (-rotateX, transform.localEulerAngles.y, 0);
		}
	}
	void playVortex() {
		if (vortex.center.x >= vortex_end) {
			Vector2 temp1 = vortex.center;
			temp1.x = vortex_start;
			vortex.center = temp1;
		}
		Vector2 temp_end = vortex.center;
		temp_end.x = vortex_end;
		vortex.center = Vector2.MoveTowards (vortex.center, temp_end, vortex_duration * Time.deltaTime);
	}
	void OnTriggerEnter(Collider hit) {
		if (hit.gameObject.tag == "Water") {
			is_underwater = true;
			vortex.enabled = true;
			fisheye.enabled = true;
			edgeDetect.enabled = true;
			sunShaft.enabled = true;
			projector.enabled = true;
			audioController.playLoop (7);
			player.GetComponent<PlayerController> ().fallingGravity_fix = 0.5f;
			player.GetComponent<Rigidbody> ().drag = 0.3f;
			player.GetComponent<PlayerController> ().multiJump = true;
			water_front.GetComponent<MeshRenderer> ().enabled = false;
			water_back.GetComponent<MeshRenderer> ().enabled = true;
			switch (GameManager.chosen_level) {
			case 1: 
				fog.heightDensity = 0.5f;
				foreach (ParticleSystem particles in environment_particles) {
					particles.Stop ();
					particles.Clear ();
				}
				environment_particles [0].Play ();
				break;
			case 2:
				fog.height = 470f;
				foreach (ParticleSystem particles in environment_particles) {
					particles.Stop ();
					particles.Clear ();
				}
				environment_particles [6].Play ();
				break;
			}
		} else if (hit.gameObject.tag == "env_forest") {
			foreach (ParticleSystem particles in environment_particles) {
				particles.Stop ();
			}
			environment_index = 4;
			environment_particles [4].Play ();
		} else if (hit.gameObject.tag == "env_rain") {
			foreach (ParticleSystem particles in environment_particles) {
				particles.Stop ();
			}
			environment_index = 3;
			environment_particles [3].Play ();
		} else if (hit.gameObject.tag == "env_snow") {
			RenderSettings.skybox = boss_sky;
			foreach (ParticleSystem particles in environment_particles) {
				particles.Stop ();
			}		
		} else if (hit.gameObject.tag == "env_storm") {
			foreach (ParticleSystem particles in environment_particles) {
				particles.Stop ();
			}
			environment_index = 7;
			environment_particles [7].Play ();
		} else if (hit.gameObject.tag == "env_metor") {
			foreach (ParticleSystem particles in environment_particles) {
				particles.Stop ();
			}
			environment_index = 8;
			environment_particles [8].Play ();
		} else if (hit.gameObject.tag == "env_boss") {
			foreach (ParticleSystem particles in environment_particles) {
				particles.Stop ();
			}
			player.GetComponent<PlayerController> ().in_boss_area = true;
			environment_index = 9;
			environment_particles [9].Play ();
			Destroy (hit.gameObject, 4f);
		} else if (hit.gameObject.tag == "direction") {
			directional_arrow.transform.parent.GetComponent<LookAtTarget> ().target = hit.gameObject.GetComponent<Direction> ().nextTarget;
			direction_anim.SetTrigger ("A_open");
			audioController.playOnce (8);
		}
	}

	void OnTriggerExit(Collider hit) {
		if (hit.gameObject.tag == "Water") {
			is_underwater = false;
			vortex.enabled = false;
			fisheye.enabled = false;
			edgeDetect.enabled = false;
			sunShaft.enabled = false;
			projector.enabled = false;
			audioController.stopAudio ();
			glassWater.trigger_justExitWater ();
			player.GetComponent<PlayerController> ().fallingGravity_fix = 10f;
			player.GetComponent<Rigidbody> ().drag = 0f;
			player.GetComponent<PlayerController> ().multiJump = false;
			water_front.GetComponent<MeshRenderer> ().enabled = true;
			water_back.GetComponent<MeshRenderer> ().enabled = false;
			switch (GameManager.chosen_level) {
			case 1: 
				fog.heightDensity = 3f;
				foreach (ParticleSystem particles in environment_particles) {
					particles.Stop ();
					particles.Clear ();
				}
				lastParticle = environment_particles [environment_index];
				lastParticle.Simulate (lastParticle.duration);
				lastParticle.Play ();
				break;
			case 2:
				fog.height = 500f;
				foreach (ParticleSystem particles in environment_particles) {
					particles.Stop ();
					particles.Clear ();
				}
				lastParticle = environment_particles [environment_index];
				lastParticle.Simulate (lastParticle.duration);
				lastParticle.Play ();
				break;
			}
		} else if (hit.gameObject.tag == "env_snow") {
			environment_index = 5;
			environment_particles [5].Stop ();
			environment_particles [5].Play ();
			water.transform.parent = boss.transform;
			GameObject.FindGameObjectWithTag ("levelManager").GetComponent<AudioController> ().playLoop (2);
			bossPackage.SetActive (true);
		} else if (hit.gameObject.tag == "env_preBoss") {
			//RenderSettings.fog = true;
			RenderSettings.skybox = boss_sky;
			RenderSettings.skybox.SetColor ("_Tint", color_preBoss_skybox);
			RenderSettings.skybox.SetFloat ("_Exposure", 3.14f);
			currentExposure = 3.14f;
			currentColor = color_preBoss_skybox;
			bossPackage.SetActive (true);
			GameObject.FindGameObjectWithTag ("levelManager").GetComponent<AudioController> ().playLoop (4);
		} else if (hit.gameObject.tag == "direction") {
			direction_anim.SetTrigger ("A_closed");
		}
	}

	void OnTriggerStay(Collider hit) {
		if (hit.gameObject.tag == "env_forest") {
			RenderSettings.ambientLight = Color.Lerp (RenderSettings.ambientLight, color_forest, color_duration * Time.deltaTime);
			RenderSettings.ambientIntensity = Mathf.Lerp (RenderSettings.ambientIntensity, 1.5f, color_duration * Time.deltaTime);
		} else if (hit.gameObject.tag == "env_rain") {
			RenderSettings.ambientLight = Color.Lerp (RenderSettings.ambientLight, color_rain, color_duration * Time.deltaTime);
			RenderSettings.ambientIntensity = Mathf.Lerp (RenderSettings.ambientIntensity, 1f, color_duration * Time.deltaTime);
		} else if (hit.gameObject.tag == "env_snow") {
			RenderSettings.ambientLight = Color.Lerp (RenderSettings.ambientLight, color_snow, color_duration * Time.deltaTime);
			RenderSettings.ambientIntensity = Mathf.Lerp (RenderSettings.ambientIntensity, 1.5f, color_duration * Time.deltaTime);
		} else if (hit.gameObject.tag == "env_metor") {
			RenderSettings.ambientLight = Color.Lerp (RenderSettings.ambientLight, color_metor, color_duration * Time.deltaTime);
			RenderSettings.ambientIntensity = Mathf.Lerp (RenderSettings.ambientIntensity, 1.5f, color_duration * Time.deltaTime);
			RenderSettings.skybox.SetFloat ("_Exposure", currentExposure);
			currentExposure = Mathf.MoveTowards (currentExposure, 1.11f, color_duration * Time.deltaTime);
			RenderSettings.skybox.SetColor ("_Tint", currentColor);
			currentColor = Color.Lerp (currentColor, color_metor_skyBox, color_duration * Time.deltaTime);
		} else if (hit.gameObject.tag == "env_boss") {
			RenderSettings.ambientLight = Color.Lerp (RenderSettings.ambientLight, color_boss, color_duration * Time.deltaTime);
			RenderSettings.ambientIntensity = Mathf.Lerp (RenderSettings.ambientIntensity, 3f, color_duration_preBoss * Time.deltaTime);
			RenderSettings.skybox.SetFloat ("_Exposure", currentExposure);
			currentExposure = Mathf.MoveTowards (currentExposure, 5.7f, color_duration * Time.deltaTime);
			RenderSettings.skybox.SetColor ("_Tint", currentColor);
			currentColor = Color.Lerp (currentColor, color_boss_skybox, color_duration* Time.deltaTime);
			RenderSettings.fogColor = Color.Lerp (RenderSettings.fogColor, color_bossFog, color_duration * Time.deltaTime);
		} else if (hit.gameObject.tag == "env_preBoss") {
			RenderSettings.ambientLight = Color.Lerp (RenderSettings.ambientLight, color_preBoss, color_duration_preBoss * Time.deltaTime);
			RenderSettings.ambientIntensity = Mathf.Lerp (RenderSettings.ambientIntensity, 2.5f, color_duration_preBoss * Time.deltaTime);
			RenderSettings.skybox.SetFloat ("_Exposure", currentExposure);
			currentExposure = Mathf.MoveTowards (currentExposure,  0f, color_duration_preBoss * Time.deltaTime);
			GetComponent<Camera> ().farClipPlane = Mathf.Lerp (GetComponent<Camera> ().farClipPlane, 900f, color_duration_preBoss * Time.deltaTime);
		} else if (hit.gameObject.tag == "env_rock") {
			RenderSettings.fogColor = Color.Lerp (RenderSettings.fogColor, color_rock, color_duration * Time.deltaTime);
		}
	}


	public void playTriggerAnim(string animation) { anim.SetTrigger (animation); }
	public void resetTriggerAnim(string animation) { anim.ResetTrigger (animation); }
	public void setAnimatornActive(bool result) { anim.enabled = result; }
	public void playAudioOnce(int index) { audioController.playOnce (index); }
	public void playAudioLoop(int index) { audioController.playLoop (index); }
	public void stopAudio() { audioController.stopAudio (); }
	public void resetAnim(string animation) { anim.Play(animation, 0, 0f); } 
	public void setDamp(float amount) { this.damp = amount; }
	public void unparent() { transform.parent = null; }
	public void set_isAimming(bool result) { is_aimming = result; }
	public void lerpFOW(float target) { GetComponent<Camera> ().fieldOfView = target; }
	public void dead_camera() { 
		is_dead = true; 
		//edgeDetect.sampleDist = 1f;

		edgeDetect.lumThreshold = 0f;
		edgeDetect.sampleDist = 0f;
	}
}
