﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Mushroom : MonoBehaviour {

	public EnemyHealth_manager health;
	public GameObject spawnpoint;
	public GameObject shoot_prefab;
	public GameObject skin;
	public float shoot_distance_min = 27f;
	public float shoot_distance_max = 35f;
	public float chase_distance = 120f;
	public float shoot_power = 1600f;
	public Material[] materials;
	public float minScale = 0.4f;
	public float maxScale = 1.2f;

	NavMeshAgent nav;
	Animator anim;
	AudioController audioController;
	GameObject player;
	GameObject body;
	LookAtPlayer lookAtPlayer;
	float shoot_distance;
	float shoot_timer = 0f;
	float shoot_cd = 2f;
	float currentHealth;
	bool isDead = false;

	void Awake() {
		health.enemyType = 2;
	}
	void Start() {
		nav = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();
		body = transform.Find ("metarig").gameObject;
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		lookAtPlayer = GetComponent<LookAtPlayer> ();
		currentHealth = health.currentHealth;
		skin.GetComponent<SkinnedMeshRenderer>().material = materials[Random.Range (0, materials.Length)];
		transform.localScale = transform.localScale * Random.Range (minScale, maxScale);
		shoot_distance = Random.Range (shoot_distance_min, shoot_distance_max);
		nav.stoppingDistance = shoot_distance;
	}
	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				die ();
				isDead = true;
			} else {
				if (currentHealth != health.currentHealth) {
					currentHealth = health.currentHealth;
					hurt ();
				} else if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("Mushroom_hurt")) {
					float distance = Vector3.Distance (this.transform.position, player.transform.position);
					if (distance <= chase_distance) {
						if (distance <= shoot_distance) {
							if (Time.time > shoot_timer) {
								shoot ();
							}
						} else {
							chase ();
						}
					}
				}
			}
		}
	}

	void shoot () {
		shoot_timer = Time.time + shoot_cd;
		anim.SetBool("A_run", false);
		anim.SetTrigger ("A_shoot");
	}
	void instantiate() {
		GameObject stone = (GameObject)(Instantiate (shoot_prefab, spawnpoint.transform.position, Quaternion.identity));
		stone.GetComponent<Rigidbody> ().AddForce (spawnpoint.transform.forward * shoot_power);
	}
	void chase() {
		nav.SetDestination (player.transform.position);
		anim.SetBool ("A_run", true);
	}
	void hurt() {
		if (health.currentHealth >= 0) {
			anim.SetBool ("A_run", false);
			anim.SetTrigger ("A_hurt");
			audioController.playOnce (1);
		}
	}
	void die() {
		GameManager.stats_enemiesKilled += 1;
		body.GetComponent<BoxCollider> ().enabled = false;
		GetComponent<Rigidbody> ().isKinematic = false;
		lookAtPlayer.enabled = false;
		anim.SetBool("A_run", false);
		anim.SetTrigger ("A_die");
		audioController.playOnce (2);
	}
	void clearBody() {
		nav.enabled = false;
		Destroy (this.gameObject, 5f);
	}
}