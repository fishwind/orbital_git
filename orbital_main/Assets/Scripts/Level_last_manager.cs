﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;


[RequireComponent(typeof(Animator))]
public class Level_last_manager : MonoBehaviour {

	public GameObject boss;
	public GameObject bossPackage;
	public GameObject canvasResult;
	public GameObject loadingScreen;
	public List<GameObject> UI_to_be_disabled;
	public Slider loadingBar;
	public Image loadingImage;
	public Text loadingText;
	public Sprite keyboard1;
	public Sprite keyboard2;
	public Sprite keyboard0;
	public Texture2D mouseImage;
	public Material skybox_gameover;

	float fakeIncrement;
	float fakeTiming;
	bool isGameOver = false;
	bool is_dead;
	AsyncOperation async;
	GameManager gameManager;
	GameObject canvas_screenEffects;
	GameObject player;
	GameObject camera_main;
	Animator anim;
	AudioController audioController;
	float startTime;

	void Awake() {
		GameManager.chosen_level = 2;
		GameManager.spawnEverything ();
	}

	void Start() {
		Cursor.visible = false;
		Cursor.SetCursor(mouseImage, new Vector2(0,0), CursorMode.ForceSoftware);
		Screen.lockCursor = true;
		startTime = Time.time;
		GameManager.stats_missionSucceed = false;
		GameManager.stats_timeTaken = 0;		
		GameManager.stats_dmgTaken = 0;	
		GameManager.stats_manaUsed = 0;	
		GameManager.stats_enemiesKilled = 0;	
		GameManager.stats_itemCollected = 0;
		GameManager.stats_numberButton = 0;	
		GameManager.stats_numberJump = 0;
		GameManager.stats_numberAttack = 0;	
		GameManager.stats_numberDash = 0;	
		GameManager.stats_numberHurt = 0;	
		GameManager.totalScore = 0;
		GameManager.rank = "F";
		update_highscore ();
		gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
		player = GameObject.FindGameObjectWithTag ("Player");
		player.GetComponent<PlayerController>().can_shoot = true;
		//player.GetComponent<PlayerController> ().canvas_icon.gameObject.SetActive (true);
		camera_main = GameObject.FindGameObjectWithTag ("MainCamera");
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		camera_main.transform.parent = GameObject.FindGameObjectWithTag ("Player").transform.parent;
		canvas_screenEffects = GameObject.FindGameObjectWithTag ("canvas_screenEffects");
	}

	void Update() {
		if (!isGameOver) {
			GameManager.stats_timeTaken = (int)(Time.time - startTime);
		}
	}
	void LateUpdate() {
		if (!is_dead && player.GetComponent<PlayerController> ().currentHealth <= 0) {
			is_dead = true;
			StartCoroutine (gameOverLoseEffects ());
		}
	}
	public void loadLevel (int level) {
		GameManager.chosen_level = level;
		update_highscore ();
		StartCoroutine(loadLevelWithFakeRealBar());
	}

	public IEnumerator loadLevelWithFakeRealBar() {
		loadingScreen.SetActive (true);
		if (GameManager.chosen_level == 0) {
			loadingImage.sprite = keyboard0;
		} else if (GameManager.chosen_level == 1) {
			loadingImage.sprite = keyboard1;
		} else if (GameManager.chosen_level == 2) {
			loadingImage.sprite = keyboard2;
		}

		yield return new WaitForSeconds (1);
		async = SceneManager.LoadSceneAsync (GameManager.chosen_level);
		async.allowSceneActivation = false;
		while (loadingBar.value != 1f) {
			if(async.progress == 0.9f || loadingBar.value <= async.progress){
				fakeIncrement = UnityEngine.Random.Range (0.01f, 0.1f);
				fakeTiming = UnityEngine.Random.Range (0.01f, 0.5f);
				loadingBar.value += fakeIncrement;
			}
			yield return new WaitForSeconds (fakeTiming);
		}
		while(loadingBar.value == 1f) {
			if (GameManager.chosen_level != 0) {
				loadingText.text = "Press 'SPACE' to continue";
				if (Input.GetKeyDown ("space")) {
					loadingText.text = "Starting...";
					async.allowSceneActivation = true;
				}
			} else {
				loadingText.text = "Starting...";
				async.allowSceneActivation = true;
			}
			yield return null;
		}
	}
	void spawnBoss() {
		bossPackage.SetActive (true);
	}
	void update_highscore() {
		int[] highscores = new int[6];
		highscores [0] = PlayerPrefs.GetInt ("highscore_1");
		highscores [1] = PlayerPrefs.GetInt ("highscore_2");
		highscores [2] = PlayerPrefs.GetInt ("highscore_3");
		highscores [3] = PlayerPrefs.GetInt ("highscore_4");
		highscores [4] = PlayerPrefs.GetInt ("highscore_5");
		highscores [5] = PlayerPrefs.GetInt ("highscore_6");
		Array.Sort (highscores);
		PlayerPrefs.SetInt ("highscore_1", highscores [5]);
		PlayerPrefs.SetInt ("highscore_2", highscores [4]);
		PlayerPrefs.SetInt ("highscore_3", highscores [3]);
		PlayerPrefs.SetInt ("highscore_4", highscores [2]);
		PlayerPrefs.SetInt ("highscore_5", highscores [1]);
	}
	public void gameOver() {
		StartCoroutine (gameOverEffects ());
	}
	IEnumerator gameOverEffects() {
		isGameOver = true;
		GameManager.stats_missionSucceed = true;

		player.SetActive (false);
		camera_main.transform.parent = player.transform.parent;
		UI_to_be_disabled.Add (player.GetComponent<PlayerController> ().canvas_bars.gameObject);
		UI_to_be_disabled.Add (player.GetComponent<PlayerController> ().canvas_icon.gameObject);
		UI_to_be_disabled.Add (player.GetComponent<PlayerController> ().canvas_crosshair.gameObject);
		foreach (GameObject ui in UI_to_be_disabled) {
			ui.SetActive (false);
		}
		camera_main.GetComponent<Animator> ().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load ("Camera_gameOver", typeof(RuntimeAnimatorController));
		camera_main.GetComponent<Animator>().enabled = true;
		camera_main.GetComponent<Camera_Controller1> ().enabled = false;
		camera_main.GetComponent<Animator> ().SetTrigger ("A_win2");
		camera_main.GetComponent<AudioSource> ().enabled = false;
		yield return new WaitForSeconds (0.2f);
		audioController.playOnce (0);
		yield return new WaitForSeconds (0.8f);
		RenderSettings.skybox = skybox_gameover;
		yield return new WaitForSeconds(5.2f);
		audioController.playOnce (1);	// play win sound
		canvasResult.SetActive (true);
		Cursor.visible = true;
		Screen.lockCursor = false;
	}
	IEnumerator gameOverLoseEffects() {
		player.GetComponent<PlayerController> ().is_dead = true;
		canvas_screenEffects.GetComponent<Canvas_screenEffect>().fadeBlack ();
		camera_main.GetComponent<Camera_Controller1> ().dead_camera ();
		isGameOver = true;
		GameManager.stats_missionSucceed = false;
		player.GetComponent<PlayerController> ().freezePlayer ();
		player.GetComponent<PlayerController> ().enabled = false;
		camera_main.GetComponent<AudioSource> ().enabled = false;
		UI_to_be_disabled.Add (player.GetComponent<PlayerController> ().canvas_bars.gameObject);
		UI_to_be_disabled.Add (player.GetComponent<PlayerController> ().canvas_icon.gameObject);
		UI_to_be_disabled.Add (player.GetComponent<PlayerController> ().canvas_crosshair.gameObject);
		foreach (GameObject ui in UI_to_be_disabled) {
			ui.SetActive (false);
		}
		Time.timeScale = 0.4f;
		yield return new WaitForSeconds (0.8f);
		Time.timeScale = 1f;
		yield return new WaitForSeconds (0.7f);
		audioController.playOnce (2);
		canvasResult.SetActive (true);
		Cursor.visible = true;
		Screen.lockCursor = false;
	}
}
