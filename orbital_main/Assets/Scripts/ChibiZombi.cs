﻿using UnityEngine;
using System.Collections;

public class ChibiZombi : MonoBehaviour {

	public EnemyHealth_manager health;
	public float close_enough_distance = 40;
	public float chaseSpeed_min = 3f;
	public float chaseSpeed_max = 7f;
	public float jumpPower_min = 3500f;
	public float jumpPower_max = 7500f;
	public float size_min = 0.75f;
	public float size_max = 1.5f;

	GameObject player;
	Animator anim;
	Rigidbody rigidbody;
	float hurtTimer = 0f;
	float hurtDelay = 0.5f;
	float turn_delay = 0.5f;
	float chaseSpeed;
	bool close_enough = false;
	bool hasHit = false;
	bool isDead = false;
	AudioController audioController;

	void Awake() {
		health.enemyType = 2;
	}
	void Start() {
		anim = this.GetComponent<Animator> ();
		rigidbody = this.GetComponent<Rigidbody> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		chaseSpeed = Random.Range (chaseSpeed_min, chaseSpeed_max);
		transform.localScale = transform.localScale * Random.Range (size_min, size_max);
		audioController = GetComponent<AudioController> ();
	}

	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			}
			if (close_enough /* && angleDifferent < 35 */) {
				Vector3 vectorDifferent = player.transform.position - this.transform.position;
				vectorDifferent.y = 0;
				transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (vectorDifferent), turn_delay);
				float angleDifferent = Vector3.Angle (vectorDifferent, transform.forward);
				if (Vector3.Distance (transform.position, player.transform.position) <= 1) {
					attacking (); 
				} else {
					chasing ();
				}
				if (Time.time > hurtTimer) {
					hurtTimer = Time.time + hurtDelay;
					hasHit = false;
				}
			}
		}
	}
	void FixedUpdate() {
		checkDistance ();
	}
	void checkDistance() {
		if (Vector3.Distance(player.transform.position, this.transform.position) <= close_enough_distance) {
			if(!close_enough) anim.SetBool ("A_run", true);
			close_enough = true;
		} else {
			if (close_enough) {
				anim.SetBool ("A_run", false);
				anim.SetTrigger ("A_walk");
			}
			close_enough = false;
		}
	}
	void chasing() {
		transform.Translate(Vector3.forward * chaseSpeed * Time.deltaTime);
		anim.SetBool ("A_attacking", false);
		anim.SetBool ("A_run", true);
	}
	void attacking() {
		anim.SetBool ("A_run", false);
		anim.SetBool ("A_attacking", true);
	}
	void OnCollisionEnter(Collision hit) {
		if (isDead == false && hit.gameObject.tag == "ground") {
			rigidbody.AddForce (Vector3.up * Time.deltaTime * Random.Range (jumpPower_min, jumpPower_max));
		}
	}
	void OnTriggerStay(Collider hit) {
		if (isDead == false && hasHit == false && hit.gameObject.tag == "Player") {
			hasHit = true;
			hit.gameObject.GetComponent<PlayerController> ().player_hurt_small ();
		}
	}
	void die() {
		anim.ResetTrigger ("A_walk");
		anim.SetBool ("A_run", false);
		anim.SetBool ("A_attacking", false);
		anim.SetTrigger ("A_die");
		clearBody ();
	}
	void clearBody() {
		foreach (Collider collider in health.GetComponents<Collider>()) {
			collider.enabled = false;
		}
		GameManager.stats_enemiesKilled += 1;
	}
}
