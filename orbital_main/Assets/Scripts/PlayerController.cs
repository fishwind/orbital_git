﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


[RequireComponent(typeof (Animator))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioController))]
public class PlayerController : MonoBehaviour {

	// Player setup keys
	public string forward_key = "w", backward_key = "s", leftward_key = "a", rightward_key = "d", jump_key = "space", prevWeapon_key = "q", nextWeapon_key = "e", duck_key = "left ctrl";

	// Editable parameters
	public List<GameObject> bulletList;		// a list of weapons that player owns
	public GameObject bullet_teleport;
	public GameObject bullet_1; 
	public GameObject bullet_ultra;
	public GameObject bullet_laser;
	public GameObject ultra_beam;
	public GameObject wave;
	public GameObject particle_flash;
	public GameObject particle_jump;
	public GameObject particle_jump_duck;
	public GameObject particle_dashS;
	public GameObject[] particle_dashF;
	public GameObject particle_troll;
	public GameObject troll_spawnpoint;
	public GameObject particle_landed;
	public GameObject lightingHit_prefab;
	public Material sode_material;
	public ParticleSystem weapon_buff;	// color ful
	public LineRenderer lighting_line;
	public Light lighting_light;
	public ParticleSystem value_buff;	// green and blue
	public ParticleSystem lighting_buff;
	public ParticleSystem wind_buff;
	public ParticleSystem fire_buff;
	public ParticleSystem energy_buff;
	public ParticleSystem lighting_buff_shoot_particle;
	public Sprite color_ultraIcon;
	public Sprite black_ultraIcon;
	public Animator anim_screen;
	public Image ultraBullet_icon;
	public Canvas_crosshair canvas_crosshair;
	public Canvas_iconBullet canvas_icon;
	public Canvas_screenEffect canvas_screenEffect;
	public Canvas_bars canvas_bars;
	public Canvas_tips canvas_tips;
	public Canvas_buff canvas_buff;
	public Damage_sphere_small damage_sphere_small;
	public Damage_sphere_mid damage_sphere_mid;
	public Damage_sphere_big damage_sphere_big;
	public int maxSpeed_normal = 6;
	public int maxSpeed_special = 100;
	public int movement_power = 90;
	public int dashing_power = 1000;
	public int dashing_power_sides = 500;
	public int jump_power = 1000;
	public int jump_power_duck = 2000;
	public int jump_power_duck_directional = 1000;
	public float maxHealth = 1000f;
	public float maxMana = 1000f;
	public float mana_per_sec = 0.5f;
	public float mana_consume_trollDash1 = 15f;
	public float mana_consume_trollDash2 = 10f;
	public float mana_consume_bullet1 = 20f;
	public float mana_consume_ultra = 350f;
	public float mana_consume_lazer = 5f;
	public float dash_duration_Type1 = 0.4f;
	public float dash_duration_Type2 = 0.15f;
	public float dash_duration_Type3 = 0.3f;
	public float mouseX_sensitivity = 3.0f;
	public float bulletPower_1 = 4000;
	public float bulletPower_ultra = 5500;
	public float playerGravity = 1.5f;	// will auto change depending if player reaches his max speed
	public float duckingGravity = 30f;
	public float fallingGravity_fix = 10f;
	public float buff_duration = 30f;
	public float buff_timer = 0f;	
	public bool is_buff_fire = false;
	public bool is_buff_lighting = false;
	public bool is_buff_wind = false;
	public bool multiJump = false;
	public bool enableMove = true;
	public bool can_shoot = false;
	public bool in_boss_area = false;
	public bool is_dead = false;
	public float currentHealth;
	public float currentMana;

	// private attributes
	Animator anim;
	Animator anim_leftHand;
	Rigidbody rigidbody;
	GameObject laser_prefab1 = null;	// to be used for laser only
	GameObject laser_prefab2 = null;	// to be used for laser only
	GameObject tele_prefab = null;		// to be used for tele only
	AudioController audioController;
	Camera_Controller1 camera_main;		
	Ray lightingRay;
	RaycastHit lightingHit;
	ShakeCam shakeCam;
	int currentBullet_index = 0;		// currenting using which weapon
	int bullet_ultra_amount = 3;
	float laserAudio_timer = 0f;
	float currentSpeed;
	float currentDirectionSpeed;
	float dash_duration;
	float doubleTap_cooldown = 0.5f;	// use for dashing
	float doubleTap_time = 0;			// use for dashing
	float dashTimer = 0f;
	float duckTimer = 0f;
	float dashDelay = 0.4f;
	float lighting_buff_timer = 0;
	char doubleTap_sign = 'E';			// use for dashing     (E = Empty, F< = Forward, L = leftward, R = Rightward)
	char prevDash = 'E';
	bool can_tele = false;
	bool is_charging_ultraBullet = false;
	bool require_mana = true;
	float trollTimer = 0f;
	bool w_press = false, s_press = false, a_press = false, d_press = false;
	// for attacking animations
	bool hasQ = false;
	bool is_attacking = false;
	bool is_dashing = false;
	bool is_grounded = true;
	bool is_aimming = false;
	bool is_leftUp = false;
	//bool is_lasering  = false;
	//float laser_timer = 0f;
	float att_timer = 0f;
	float att_cd = 0.02f;
	float disableTimer = 0f;
	float disableDuration = 1.15f;

	/*__________________________________________________________________________________________________________________________________________________________________________________________________*/
	void Start () {
		anim = GetComponent<Animator> ();
		rigidbody = GetComponent<Rigidbody> ();
		audioController = GetComponent<AudioController> ();
		camera_main = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera_Controller1> ();
		rigidbody.freezeRotation = true;
		currentHealth = maxHealth;
		currentMana = maxMana;
		shakeCam = GetComponent<ShakeCam> ();
		buff_timer = 0f;
		if (GameManager.chosen_level == 2) {
			anim_leftHand = GameObject.FindGameObjectWithTag ("lefthand").GetComponent<Animator>();
			if (GameManager.chosen_character == 7) {
				camera_main.unityChan_sodecandyR.GetComponent<SkinnedMeshRenderer> ().material = sode_material;
				camera_main.unityChan_sodecandyL.GetComponent<SkinnedMeshRenderer> ().material = sode_material;
			} else {
				camera_main.unityChan_sodeR.GetComponent<SkinnedMeshRenderer>().material = sode_material;
				camera_main.unityChan_sodeL.GetComponent<SkinnedMeshRenderer>().material = sode_material;
			}
		}
	}
	void Update() {
		//////////////////////////////////////////////////////   hacks
		if (Input.GetKeyDown ("m")) {
			//currentHealth = maxHealth;
			//currentMana = maxMana;
		}
		if (Input.GetKeyDown ("o")) {
			//currentHealth = 0f;
			//currentMana = 0f;
		}
		if (Input.GetKeyDown ("p")) {
			//update_bulletUltra (1);
		}
		/////////////////////////////////////////////////////////   hacks
	

		if (Time.time > lighting_buff_timer) {
			lighting_line.enabled = false;
			lighting_light.enabled = false;
		}
		if (buff_timer <= 0f && check_is_buffing ()) {
			is_buff_fire = false;
			is_buff_lighting = false;
			is_buff_wind = false;
			canvas_buff.gameObject.SetActive (false);
			fire_buff.Stop ();
			lighting_buff.Stop ();
			wind_buff.Stop ();
		} else {
			buff_timer -= Time.deltaTime;
		}
		if (is_buff_wind) {
			movement_power = 200;
		} else {
			movement_power = 50;
		}
		if (is_buff_fire) {
			damage_sphere_small.hit_dmg = 15f;
			damage_sphere_small.currentParticle = damage_sphere_small.spinParticle1__big_prefab;
			damage_sphere_small.gameObject.GetComponent<SphereCollider> ().radius = 2.5f;
		} else {
			damage_sphere_small.hit_dmg = 10f;
			damage_sphere_small.currentParticle = damage_sphere_small.spinParticle1_prefab;
			damage_sphere_small.gameObject.GetComponent<SphereCollider> ().radius = 0.85f;
		}


		if (Time.time < dash_duration) {
			is_dashing = true;
		} else {
			is_dashing = false;
		}
		if(!is_dashing) {
			limits_normalSpeed ();
		} else {
			limits_specialSpeed ();
		}
		if(enableMove) playerMove ();
		currentSpeed = rigidbody.velocity.magnitude;
		currentDirectionSpeed = transform.InverseTransformDirection (rigidbody.velocity).x;
		if (currentHealth < 0) currentHealth = 0;
		if (currentHealth > maxHealth) currentHealth = maxHealth;
		if (currentMana < 0) currentMana = 0;
		if (currentMana > maxMana) currentMana = maxMana;
		if (is_aimming && bulletList [currentBullet_index] == bullet_ultra) {
			if (!is_leftUp) {
				is_leftUp = true;
				anim_leftHand.SetTrigger ("A_liftUp");
			}
		} else {
			if (is_leftUp) {
				is_leftUp = false;
				anim_leftHand.SetTrigger ("A_liftDown");
			}
		}
	}
	void FixedUpdate() {
		if (!isDucking ()) {
			fixGravity ();		// fix gravity when moving normally
		} else {
			rigidbody.AddForce (Vector3.up * -1 * duckingGravity);	// ducking's gravity
		}
		currentMana += mana_per_sec;
		Quaternion fixedRotation = Quaternion.Euler (0f, transform.localEulerAngles.y, 0f);
		transform.localRotation = Quaternion.Lerp (transform.localRotation, fixedRotation, Time.deltaTime * 2f);
	}
	void fixGravity() {
		rigidbody.AddForce (Vector3.up * -1 * playerGravity);
	}
	void OnCollisionEnter(Collision hit) {
		if (hit.gameObject.tag == "ground") {
			if (!isDucking () && !is_dead) {
				anim.ResetTrigger ("A_landed");	
				anim.SetTrigger ("A_landed");	
			} else {
				Instantiate (particle_landed, transform.position, Quaternion.identity);
			}
		}
	}
	void OnTriggerEnter(Collider hit) {
		if (hit.gameObject.tag == "buff_ultra") {
			GameManager.stats_itemCollected += 1;
			update_bulletUltra (1);
			audioController.playOnce (14);
			Destroy (hit.gameObject);
		}
		if (hit.gameObject.tag == "buff_value") {
			GameManager.stats_itemCollected += 1;
			healBoth ();
			audioController.playOnce (15);
			Destroy (hit.gameObject);
		}
		if (hit.gameObject.tag == "buff_lighting") {
			GameManager.stats_itemCollected += 1;
			buff_timer = Time.time + buff_duration;
			buff_lighting ();
			audioController.playOnce (16);
			Destroy (hit.gameObject);
		}
		if (hit.gameObject.tag == "buff_fire") {
			GameManager.stats_itemCollected += 1;
			buff_timer = Time.time + buff_duration;
			buff_fire ();
			audioController.playOnce (17);
			Destroy (hit.gameObject);
		}
		if (hit.gameObject.tag == "buff_wind") {
			GameManager.stats_itemCollected += 1;
			buff_timer = Time.time + buff_duration;
			buff_wind ();
			audioController.playOnce (18);
			Destroy (hit.gameObject);
		}
		if (hit.gameObject.tag == "buff_energy") {
			GameManager.stats_itemCollected += 1;
			buff_energy();
			audioController.playOnce (19);
			Destroy (hit.gameObject);
		}
		if (hit.gameObject.tag == "tips_giver") {
			canvas_tips.showTips (hit.gameObject.GetComponent<Tips_giver> ().tips_index);
			Destroy (hit.gameObject);
		}
		if (hit.gameObject.tag == "Water") {
			canvas_tips.showTips (3);
		}
	}
	void OnTriggerStay(Collider hit) {
		if (hit.gameObject.tag == "ground") is_grounded = true; 
	}
	void OnTriggerExit(Collider hit) {
		if (hit.gameObject.tag == "ground") is_grounded = false;
	}
	void limits_normalSpeed() {
		if (rigidbody.velocity.magnitude > maxSpeed_normal) { 
			rigidbody.velocity = Vector3.Lerp (rigidbody.velocity, rigidbody.velocity.normalized * maxSpeed_normal, 0.4f);
			playerGravity = fallingGravity_fix;
		} else {
			playerGravity = 1f;
		}
	}
	void limits_specialSpeed() {
		if (rigidbody.velocity.magnitude > maxSpeed_special) { 
			rigidbody.velocity = rigidbody.velocity.normalized * maxSpeed_special;
			playerGravity = 10f;
		} else {
			playerGravity = 1f;
		}
		if(rigidbody.velocity.magnitude < maxSpeed_normal) { is_dashing = false; }
	}

	/*__________________________________________________________________________________________________________________________________________________________________________________________________*/


	void playerMove() {
		anim.SetFloat ("A_speed", currentSpeed);
		anim.SetFloat ("A_directionSpeed", currentDirectionSpeed);
		transform.Rotate(0, Input.GetAxis("Mouse X") * mouseX_sensitivity, 0);	// Mouse X is default player input mouse setting.

		if (can_shoot && Input.GetMouseButtonDown (1)) move_setAim();
		if (can_shoot && Input.GetMouseButtonUp (1) && !is_charging_ultraBullet) move_resetAim ();
	
		if (can_shoot && is_aimming && Input.GetMouseButtonDown (0)) {
			att_timer = Time.time + att_cd;
			if (!can_tele) {
				move_shoot1 ();
			} else {
				move_teleport ();
			}
		}
		if (can_shoot && is_aimming && bulletList[currentBullet_index] == bullet_laser) {
			if (Input.GetMouseButton (0)) move_shoot_laser ();
			if (Input.GetMouseButtonUp (0) && laser_prefab1 != null) {
				//if (is_lasering) {
				//	laser_timer = Time.time + 0.5f;
				//	is_lasering = false;
				//}
				canvas_crosshair.playTriggerAnim ("A_crosshair_reset");
				camera_main.playTriggerAnim ("A_camera_reset");
				camera_main.stopAudio ();
				laser_prefab1.GetComponent<BeamParam>().bEnd = true;
				laser_prefab2.GetComponent<BeamParam>().bEnd = true;
			}
		}
		if(!is_aimming && Input.GetMouseButtonDown (0) && Time.time > att_timer) { 	// if not aimming, left click to melee
			att_timer = Time.time + att_cd;
			anim.ResetTrigger ("A_landed");
			if (!is_attacking) {
				is_attacking = true;
				move_attack ();
			} else {
				if (!anim.GetCurrentAnimatorStateInfo (0).IsTag ("attackMoves")) {
					move_attack ();
				} else {
					hasQ = true;
				}
			}
		}
		if(Input.GetKey(forward_key)) move_forward();
		if(Input.GetKeyDown(forward_key)) move_dashForward();
		if(Input.GetKeyUp(forward_key)) w_press = false;

		if(Input.GetKey(backward_key)) move_backward();
		if(Input.GetKeyDown(backward_key)) move_dashBackward();
		if(Input.GetKeyUp(backward_key)) s_press = false;
	
		if(Input.GetKey(leftward_key)) move_leftward();
		if(Input.GetKeyDown(leftward_key)) move_dashLeftward();
		if(Input.GetKeyUp(leftward_key)) a_press = false;

		if(Input.GetKey(rightward_key)) move_rightward();
		if(Input.GetKeyDown(rightward_key)) move_dashRightward();
		if(Input.GetKeyUp(rightward_key)) d_press = false;

		if(Input.GetKeyDown(jump_key)) move_jump();

		if(can_shoot && Input.GetKeyDown(prevWeapon_key)) move_preWeapon();
		if(can_shoot && Input.GetKeyDown(nextWeapon_key)) move_nextWeapon();

		if (currentMana > mana_consume_trollDash1 && Input.GetKeyDown ("r")) {
			currentMana -= mana_consume_trollDash1;
			GameManager.stats_manaUsed += (int)(mana_consume_trollDash1);
			GameManager.stats_numberButton += 1;
			GameManager.stats_numberDash += 1;
			GameManager.stats_numberJump += 1;
			rigidbody.AddRelativeForce (Vector3.up * jump_power_duck * 0.2f);
			GameObject jump_particle = (GameObject)(Instantiate (particle_jump, transform.position, transform.rotation));
			jump_particle.transform.parent = this.transform;
			if (is_aimming) {
				move_resetAim ();
			}
		}
		if (Input.GetKey ("r") && !is_aimming) { 
			move_IwannaBeAhero ();
		} else {
			GetComponent<Rigidbody> ().collisionDetectionMode = CollisionDetectionMode.Discrete;
			ParticleSystem.EmissionModule em = particle_troll.GetComponent<ParticleSystem> ().emission;
			em.enabled = false;
		}
		if (Input.GetKey (duck_key)) move_duck ();
		if (Input.GetKeyUp (duck_key)) move_unduck ();
	}

	/*__________________________________________________________________________________________________________________________________________________________________________________________________*/

	void move_IwannaBeAhero() {
		GetComponent<Rigidbody> ().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
		if (currentMana > mana_consume_trollDash2 && Time.time > trollTimer && doubleTap_sign == 'F' && Time.time < doubleTap_time) {
			currentMana -= mana_consume_trollDash2;
			//Quaternion fixedRotation = Quaternion.Euler (0f, transform.localEulerAngles.y, 0f);
			//transform.localRotation = fixedRotation;
			GameManager.stats_manaUsed += (int)(mana_consume_trollDash2);
			anim.SetTrigger ("A_dash");
			anim.SetFloat ("A_dashingType", 0);
			audioController.playOnce (Random.Range (3, 5));
			rigidbody.AddRelativeForce (Vector3.forward * dashing_power* 1.25f);
			trollTimer = Time.time + att_cd;
			doubleTap_sign = 'E';
			ParticleSystem.EmissionModule em = particle_troll.GetComponent<ParticleSystem> ().emission;
			em.enabled = true;
		} else {
			doubleTap_sign = 'F';
			doubleTap_time = doubleTap_cooldown + Time.time;
		}
	}

	void move_forward() { 
		if (!isDucking ()) { rigidbody.AddRelativeForce (Vector3.forward * movement_power * 1); } 
	} 
	void move_backward() { 
		if (!isDucking ()) { rigidbody.AddRelativeForce (Vector3.forward * movement_power * -0.75f); }
	}
	void move_leftward() {
		if (!isDucking ()) { rigidbody.AddRelativeForce (Vector3.left * movement_power * 0.9f); }
	}
	void move_rightward() { 
		if (!isDucking ()) { rigidbody.AddRelativeForce (Vector3.left * movement_power * -0.9f); } 
	}
	public void move_helicoperJump() {
		anim.ResetTrigger ("A_landed");	
		anim.SetTrigger ("A_jump");
		rigidbody.AddRelativeForce (Vector3.up * jump_power * 0.25f);
		rigidbody.AddRelativeForce (Vector3.forward * jump_power * 0.9f);
		dash_duration = Time.time + dash_duration_Type2;
		audioController.playOnce (0);
		GameObject jump_particle = (GameObject)(Instantiate (particle_jump, transform.position, transform.rotation));
		jump_particle.transform.parent = this.transform;
	}
	void move_jump() {
		GameManager.stats_numberButton += 1;
		if (is_grounded || multiJump) {
			GameManager.stats_numberJump += 1;
			is_grounded = false;
			anim.ResetTrigger ("A_landed");	
			anim.SetTrigger ("A_jump");
			if (!isDucking ()) {
				rigidbody.AddRelativeForce (Vector3.up * jump_power);
				dash_duration = Time.time + dash_duration_Type2;
				audioController.playOnce (0);
				GameObject jump_particle = (GameObject)(Instantiate (particle_jump, transform.position, transform.rotation));
				jump_particle.transform.parent = this.transform;
			} else {
				anim.SetBool("A_duck", false);
				duckTimer = Time.time + dashDelay;
				dash_duration = Time.time + dash_duration_Type3;
				audioController.playOnce (8);
				GameObject jump_particle_duck = (GameObject)(Instantiate (particle_jump_duck, transform.position, transform.rotation));
				jump_particle_duck.transform.parent = this.transform;
				damage_sphere_big.openSphere ();
				if (is_aimming) { move_resetAim (); }
				if (w_press && a_press && !s_press && !d_press) {			// w a
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.forward * jump_power_duck_directional);
					rigidbody.AddRelativeForce (Vector3.left * jump_power_duck_directional);
				} else if (w_press && d_press && !a_press && !s_press) {	// w d
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.forward * jump_power_duck_directional);
					rigidbody.AddRelativeForce (Vector3.right * jump_power_duck_directional);
				} else if (s_press && a_press && !w_press && !d_press) {	// s a
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.forward * -jump_power_duck_directional * 0.5f);
					rigidbody.AddRelativeForce (Vector3.left * jump_power_duck_directional);
				} else if (s_press && d_press && !w_press && !a_press) {	// s d
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.forward * -jump_power_duck_directional * 0.5f);
					rigidbody.AddRelativeForce (Vector3.right * jump_power_duck_directional);
				} else if (w_press && !a_press && !s_press && !d_press) {	// w
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.forward * jump_power_duck_directional * 1.25f);
				} else if (s_press && !a_press && !w_press && !d_press) {	// s
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.forward * -jump_power_duck_directional * 1.25f * 0.5f);
				} else if (a_press && !w_press && !s_press && !d_press) {	// a
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.left * jump_power_duck_directional * 1.25f);
				} else if (d_press && !w_press && !s_press && !a_press) {	// d
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck);
					rigidbody.AddRelativeForce (Vector3.right * jump_power_duck_directional * 1.25f);
				} else {
					rigidbody.AddRelativeForce (Vector3.up * jump_power_duck * 1.5f);
				}
			}
		}
	}
	void move_dashForward() {
		w_press = true;
		GameManager.stats_numberButton += 1;
		if (doubleTap_sign == 'F' && Time.time < doubleTap_time) {
			if (prevDash != 'F' || prevDash == 'F' && Time.time > dashTimer) {
				dash_duration = Time.time + dash_duration_Type1;
				anim.SetTrigger ("A_dash");
				anim.SetFloat ("A_dashingType", 0);
				audioController.playOnce (Random.Range (3, 5));
				GameManager.stats_numberDash += 1;
				rigidbody.AddRelativeForce (Vector3.forward * dashing_power);
				doubleTap_sign = 'E';
				prevDash = 'F';
				dashTimer = Time.time + dashDelay;
				foreach (GameObject particle in particle_dashF) {
					particle.GetComponent<ParticleSystem> ().Stop ();
					particle.GetComponent<ParticleSystem> ().Play ();
				}
				if (is_aimming) { move_resetAim (); }
			}
			damage_sphere_mid.openSphere ();
		} else {
			doubleTap_sign = 'F';
			doubleTap_time = doubleTap_cooldown + Time.time;
		}
	}
	void move_dashBackward() {
		s_press = true;
		GameManager.stats_numberButton += 1;
		if (doubleTap_sign == 'B' && Time.time < doubleTap_time) {
			if (prevDash != 'B' || prevDash == 'B' && Time.time > dashTimer) {
				dash_duration = Time.time + dash_duration_Type1;
				anim.SetTrigger ("A_dash");
				anim.SetFloat ("A_dashingType", 2);
				audioController.playOnce (2);
				GameManager.stats_numberDash += 1;
				rigidbody.AddRelativeForce (Vector3.forward * -dashing_power_sides);
				doubleTap_sign = 'E';
				prevDash = 'B';
				dashTimer = Time.time + dashDelay;
				particle_dashS.GetComponent<ParticleSystem> ().Stop ();
				particle_dashS.GetComponent<ParticleSystem> ().Play ();
				if (is_aimming) { move_resetAim (); }
			}
		} else {
			doubleTap_sign = 'B';
			doubleTap_time = doubleTap_cooldown + Time.time;
		}
	}
	void move_dashLeftward() {
		a_press = true;
		GameManager.stats_numberButton += 1;
		if (doubleTap_sign == 'L' && Time.time < doubleTap_time){
			if (prevDash != 'L' || prevDash == 'L' && Time.time > dashTimer) {
				dash_duration = Time.time + dash_duration_Type1;
				anim.SetTrigger ("A_dash");
				anim.SetFloat ("A_dashingType", -1);
				audioController.playOnce (2);
				GameManager.stats_numberDash += 1;
				rigidbody.AddRelativeForce (Vector3.left * dashing_power_sides);
				doubleTap_sign = 'E';
				prevDash = 'L';
				dashTimer = Time.time + dashDelay;
				if (is_grounded) {
					particle_dashS.GetComponent<ParticleSystem> ().Stop ();
					particle_dashS.GetComponent<ParticleSystem> ().Play ();
				}
				if (is_aimming) { move_resetAim (); }
			}
		} else {
			doubleTap_sign = 'L';
			doubleTap_time = doubleTap_cooldown + Time.time;
		}
	}
	void move_dashRightward() {
		d_press = true;
		GameManager.stats_numberButton += 1;
		if (doubleTap_sign == 'R' && Time.time < doubleTap_time){
			if (prevDash != 'R' || prevDash == 'R' && Time.time > dashTimer) {
				dash_duration = Time.time + dash_duration_Type1;
				anim.SetTrigger ("A_dash");
				anim.SetFloat ("A_dashingType", 1);
				audioController.playOnce (2);
				GameManager.stats_numberDash += 1;
				rigidbody.AddRelativeForce (Vector3.left * dashing_power_sides * -1);
				doubleTap_sign = 'E';
				prevDash = 'R';
				dashTimer = Time.time + dashDelay;
				if (is_grounded) {
					particle_dashS.GetComponent<ParticleSystem> ().Stop ();
					particle_dashS.GetComponent<ParticleSystem> ().Play ();
				}
				if (is_aimming) { move_resetAim (); }
			}
		} else {
			doubleTap_sign = 'R';
			doubleTap_time = doubleTap_cooldown + Time.time;
		}
	}
	void move_preWeapon() { 
		if (!is_charging_ultraBullet) {
			GameManager.stats_numberButton += 1;
			currentBullet_index = (currentBullet_index + bulletList.Count - 1) % bulletList.Count; 
			canvas_icon.changeIcon (currentBullet_index);
			if (laser_prefab1 != null) {
				canvas_crosshair.playTriggerAnim ("A_crosshair_reset");
				camera_main.stopAudio ();
				laser_prefab1.GetComponent<BeamParam> ().bEnd = true;
				laser_prefab2.GetComponent<BeamParam> ().bEnd = true;
				laser_prefab1 = null;
				laser_prefab2 = null;
			}
			audioController.playOnce (5);
		}
	}
	void move_nextWeapon() { 
		if (!is_charging_ultraBullet) {
			GameManager.stats_numberButton += 1;
			currentBullet_index = (currentBullet_index + bulletList.Count + 1) % bulletList.Count; 
			canvas_icon.changeIcon (currentBullet_index);
			if (laser_prefab1 != null) {
				canvas_crosshair.playTriggerAnim ("A_crosshair_reset");
				camera_main.stopAudio ();
				laser_prefab1.GetComponent<BeamParam> ().bEnd = true;
				laser_prefab2.GetComponent<BeamParam> ().bEnd = true;
				laser_prefab1 = null;
				laser_prefab2 = null;
			}
			audioController.playOnce (5);
		}
	}

	/*__________________________________________________________________________________________________________________________________________________________________________________________________*/

	/// How to zoom in?: 1) place camera inside this. 2) animate it. 3) activate camera's animator. 4) tell camera we are aimming. 
	/// How to zoom out?: 1) deactivate camerai's animator. 2) reset its last animation. 3) tell camera we not aimming. 4) unparent camera. 5) shift it to player's position.
	void move_setAim() {
		GameManager.stats_numberButton += 1;
		is_aimming = true;
		camera_main.set_isAimming (true);
		camera_main.setAnimatornActive (true);
		camera_main.gameObject.transform.parent = this.transform;
		camera_main.playTriggerAnim ("A_zoom_in");
		camera_main.playAudioOnce (5);
		canvas_crosshair.setCrosshairActive (true);
		canvas_crosshair.playTriggerAnim ("A_crosshair_start");
	}
	public void move_resetAim() {
		is_aimming = false;
		is_leftUp = false;
		anim_leftHand.SetTrigger ("A_empty");
		camera_main.setAnimatornActive(false);
		camera_main.resetAnim ("camera_zoomIN");	// animation's name
		camera_main.set_isAimming (false);
		camera_main.gameObject.transform.parent = this.transform.parent;
		camera_main.lerpFOW (camera_main.defaultFOW);
		camera_main.playTriggerAnim ("A_camera_reset");
		canvas_crosshair.playTriggerAnim ("A_crosshair_reset");
		canvas_crosshair.setCrosshairActive (false);
		if (laser_prefab1 != null || laser_prefab2 != null) {
			laser_prefab1.GetComponent<BeamParam>().bEnd = true;
			laser_prefab2.GetComponent<BeamParam>().bEnd = true;
			camera_main.stopAudio ();
		}
		if (bulletList [currentBullet_index] != bullet_ultra && is_aimming == true) {
			camera_main.playAudioOnce (6);
		}
		if (tele_prefab != null) {
			Destroy (tele_prefab);
			set_canTele (false);
		}
	}
	void move_shoot1() {
		GameManager.stats_numberButton += 1;
		GameManager.stats_numberAttack += 1;
		Vector3 bulletPosition = camera_main.gameObject.transform.Find ("spawnpoint_bullet").transform.position; // bullet starting position
		Vector3 bulletPositionUltra = camera_main.gameObject.transform.Find ("spawnpoint_bullet_ultra").transform.position; // bullet starting position
		Vector3 bulletPositionLaser1 = camera_main.gameObject.transform.Find ("spawnpoint_bullet_laser1").transform.position; // bullet starting position
		Vector3 bulletPositionLaser2 = camera_main.gameObject.transform.Find ("spawnpoint_bullet_laser2").transform.position; // bullet starting position
		Vector3 bulletDirection = camera_main.gameObject.transform.forward;	// bullet flying direction is camera's facing direction
		if (bulletList [currentBullet_index] == bullet_1 && !is_charging_ultraBullet) {
			if (!require_mana || require_mana && currentMana >= mana_consume_bullet1) {
				GameObject bullet1 = (GameObject)(Instantiate (bullet_1, bulletPosition, Quaternion.identity));
				bullet1.GetComponent<Rigidbody> ().AddForce (bulletDirection * bulletPower_1);
				canvas_crosshair.playTriggerAnim ("A_crosshair_vibrateS");
				camera_main.playTriggerAnim ("A_camera_jerk");
				camera_main.playAudioOnce (0);
				if (require_mana) {
					currentMana -= mana_consume_bullet1;
					GameManager.stats_manaUsed += (int)(mana_consume_bullet1);
				}
			}
		} else if (bulletList [currentBullet_index] == bullet_ultra && !is_charging_ultraBullet) {
			is_charging_ultraBullet = true;
			GameObject bulletUltra_charge = (GameObject)(Instantiate (bullet_ultra, bulletPositionUltra, Quaternion.identity)); 
			bulletUltra_charge.transform.parent = camera_main.gameObject.transform;
			StartCoroutine (move_shoot_ultra ());
			camera_main.playAudioOnce (1);
		} else if (bulletList [currentBullet_index] == bullet_laser) {
			if (!require_mana || require_mana && currentMana >= mana_consume_lazer) {
				canvas_tips.showTips (6);
				if (in_boss_area) {
					canvas_tips.showTips (9);
				}
				if (laser_prefab1 != null || laser_prefab2 != null) {
					laser_prefab1.GetComponent<BeamParam>().bEnd = true;
					laser_prefab2.GetComponent<BeamParam>().bEnd = true;
					camera_main.stopAudio ();
				}
				//is_lasering = true;
				Quaternion rotation = camera_main.gameObject.transform.Find ("rotationpoint_bullet_laser").transform.rotation;
				GameObject wav1 = (GameObject)Instantiate (wave, bulletPositionLaser1, rotation);
				wav1.transform.Rotate (Vector3.left, 90.0f);
				GameObject wav2 = (GameObject)Instantiate (wave, bulletPositionLaser2, rotation);
				wav2.transform.Rotate (Vector3.left, 90.0f);
				wav1.GetComponent<BeamWave> ().col = this.GetComponent<BeamParam> ().BeamColor;
				wav2.GetComponent<BeamWave> ().col = this.GetComponent<BeamParam> ().BeamColor;
				laser_prefab1 = (GameObject)Instantiate (bullet_laser, bulletPositionLaser1, rotation);
				laser_prefab2 = (GameObject)Instantiate (bullet_laser, bulletPositionLaser2, rotation);
				canvas_crosshair.resetTriggerAnim ("A_crosshair_reset"); 
				canvas_crosshair.playTriggerAnim ("A_crosshair_vibrateF");
				camera_main.resetTriggerAnim ("A_camera_reset");
				camera_main.playTriggerAnim ("A_camera_laser");
				if(Time.time > laserAudio_timer) {
					audioController.playOnce (12);
					laserAudio_timer = Time.time + 1f;
				}
				camera_main.playAudioLoop (2);
				if (require_mana) {
					currentMana -= mana_consume_lazer;
					GameManager.stats_manaUsed += (int)(mana_consume_lazer);
				}
			}
		} else if (bulletList [currentBullet_index] == bullet_teleport) {
			Quaternion rotation = camera_main.gameObject.transform.rotation;
			tele_prefab = (GameObject)(Instantiate(bullet_teleport, bulletPositionLaser2, rotation));
			tele_prefab.GetComponent<Bullet_teleport> ().playerController = this;
			tele_prefab.GetComponent<Rigidbody> ().AddForce (bulletDirection * bulletPower_1);
			canvas_crosshair.playTriggerAnim ("A_crosshair_vibrateS");
			camera_main.playTriggerAnim ("A_camera_jerk");
			camera_main.playAudioOnce (3);
			set_canTele (true);
		}
	}	
	void move_shoot_laser() {
		if (!require_mana || require_mana && currentMana >= mana_consume_lazer) {
			BeamParam bp = GetComponent<BeamParam> ();
			if (laser_prefab1.GetComponent<BeamParam> ().bGero)
				laser_prefab1.transform.parent = camera_main.gameObject.transform;
			if (laser_prefab2.GetComponent<BeamParam> ().bGero)
				laser_prefab2.transform.parent = camera_main.gameObject.transform;

			Vector3 s = new Vector3 (bp.Scale, bp.Scale, bp.Scale);

			laser_prefab1.transform.localScale = s;
			laser_prefab1.GetComponent<BeamParam> ().SetBeamParam (bp);
			laser_prefab2.transform.localScale = s;
			laser_prefab2.GetComponent<BeamParam> ().SetBeamParam (bp);
			if (require_mana) {
				currentMana -= mana_consume_lazer;
				GameManager.stats_manaUsed += (int)(mana_consume_lazer);
			}

		} else {
			move_resetAim ();
		}
	}
	IEnumerator move_shoot_ultra() {
		yield return new WaitForSeconds (2);
		audioController.playOnce (13);
		yield return new WaitForSeconds (1);
		update_bulletUltra(-1);
		Vector3 bulletPosition = camera_main.gameObject.transform.Find ("spawnpoint_bullet_ultra").transform.position;
		Vector3 bulletDirection = camera_main.gameObject.transform.forward;
		Quaternion rotation = camera_main.gameObject.transform.rotation;
		GameObject bulletUltra_beam = (GameObject)(Instantiate (ultra_beam, bulletPosition, rotation));
		bulletUltra_beam.GetComponent<Rigidbody> ().AddForce (bulletDirection * bulletPower_ultra);
		is_charging_ultraBullet = false;
		shakeCam.shake_normal ();
		move_resetAim ();
	}
	void move_teleport() {
		transform.position = tele_prefab.transform.position;
		set_canTele (false);
		Instantiate (particle_flash, new Vector3 (0, 1000, 0), Quaternion.identity);
		anim.SetTrigger ("A_tele");
		audioController.playOnce (1);
		camera_main.playAudioOnce (4);
		move_resetAim ();
	}
	public void set_canTele(bool result) { this.can_tele = result; }

	/*__________________________________________________________________________________________________________________________________________________________________________________________________*/

	void endQ() {			
		if (hasQ) {
			is_attacking = true;
			move_attack ();
			hasQ = false;
		} else {
			if (enableMove == false) {
				player_hurt_big ();
			} else {
				anim.SetTrigger ("A_landed");
				is_attacking = false;
				enableMove = true;
			}
		}
	}
	void move_attack() {
		if (!s_press) {
			anim.SetTrigger ("A_spin");
			audioController.playOnce (6);
			GameManager.stats_numberButton += 1;
			GameManager.stats_numberAttack += 1;
			damage_sphere_small.openSphere ();
			if (is_buff_lighting) {
				shoot_lighting_buff ();
			}
		} else {
			anim.SetTrigger ("A_flip");
			audioController.playOnce (7);
			GameManager.stats_numberButton += 1;
			GameManager.stats_numberAttack += 1;
			damage_sphere_small.openSphere ();
			if (is_buff_lighting) {
				shoot_lighting_buff ();
			}
		}
		hasQ = false;
	}

	void move_duck() {
		if (Time.time > duckTimer) {
			anim.SetBool ("A_duck", true);
			is_dashing = true;
		}
	}
	void move_unduck() {
		GameManager.stats_numberButton += 1;
		anim.SetBool("A_duck", false);
		is_dashing = false;
	}
	bool isDucking() {
		return anim.GetBool ("A_duck");
	}
	public void player_hurt_big() {
		if (!is_dead) {
			move_resetAim ();
			can_tele = false;
			w_press = false;
			s_press = false;
			a_press = false;
			d_press = false;
			hasQ = false;
			is_attacking = false;
			is_dashing = false;
			is_aimming = false;
			anim.SetBool ("A_hurt_big", true);
			enableMove = false;
			audioController.playOnce (9);
			StartCoroutine (fix_player_recover ());
			currentHealth -= 45f;
			GameManager.stats_dmgTaken += 30;
			GameManager.stats_numberHurt += 1;
			canvas_screenEffect.trigger_hurtEffect ();
		}
	}
	public void player_hurt_small() {
		if (!is_dead) {
			audioController.playOnce (10);
			currentHealth -= 5f;
			GameManager.stats_dmgTaken += 5;
			GameManager.stats_numberHurt += 1;
			canvas_screenEffect.trigger_hurtEffect ();
		}
	}
	void player_recover() {
		if (!is_dead) {
			anim.SetBool("A_hurt_big", false);
			enableMove = true;
			move_unduck ();
		}
	}
	IEnumerator fix_player_recover() {
		yield return new WaitForSeconds (1.2f);
		if (!is_dead) {
			if (enableMove == false) {
				//anim.SetTrigger ("A_landed");
				anim.SetBool("A_hurt_big", false);
				move_unduck ();
				enableMove = true;
			}
		}
	}
	public void setEnableMove(bool result) { this.enableMove = result; }

	public void freezePlayer() {
		//move_resetAim ();
		can_tele = false;
		w_press = false;
		s_press = false;
		a_press = false;
		d_press = false;
		hasQ = false;
		is_attacking = false;
		is_dashing = false;
		is_aimming = false;
		enableMove = false;
		if (!is_dead) {
			anim.SetBool ("A_idle", true);
		} else {
			anim.ResetTrigger ("A_landed");
			anim.SetBool ("A_hurt_big", true);
			audioController.playOnce (11);
		}
	}

	public void setCurrentHealth(float result) {
		this.currentHealth = result;
	}
	public void addCurrentHealth(float result) {
		this.currentHealth += result;
	}
	public void setCurrentMana(float result) {
		this.currentMana = result;
	}
	public void addCurrentMana(float result) {
		this.currentMana += result;
	}

	/*______________________________________________________________________________________________________________________________________________________________________*/
	void update_bulletUltra (int value) {	// value = 1 or -1 ONLY
		bullet_ultra_amount += value;
		canvas_icon.update_ultraBullet (bullet_ultra_amount);
		if (bullet_ultra_amount <= 0) {
			bulletList.Remove (bullet_ultra);
			currentBullet_index = 0;
			canvas_icon.changeIcon (0);
			ultraBullet_icon.sprite = black_ultraIcon;
		} else {
			if (!bulletList.Contains (bullet_ultra)) {
				bulletList.Add (bullet_ultra);
			}
			if (value == 1) {
				weapon_buff.Stop ();
				weapon_buff.Play ();
				ultraBullet_icon.sprite = color_ultraIcon;
				canvas_tips.showTips (5);
			}
		}
	}
	void healBoth() {
		value_buff.Stop ();
		value_buff.Play ();
		currentMana += 400 ;
		currentHealth += 150;
	}

	void buff_lighting() {
		buff_timer = buff_duration;
		is_buff_lighting = true;
		canvas_buff.setType ("lighting");
		canvas_buff.gameObject.SetActive (true);
		lighting_buff.Stop ();
		lighting_buff.Play ();

	}
	void buff_fire() {
		buff_timer = buff_duration;
		is_buff_fire = true;
		canvas_buff.setType ("fire");
		canvas_buff.gameObject.SetActive (true);
		fire_buff.Stop ();
		fire_buff.Play ();

	}
	void buff_wind() {
		buff_timer = buff_duration;
		is_buff_wind = true;
		canvas_buff.setType ("wind");
		canvas_buff.gameObject.SetActive (true);
		wind_buff.Stop ();
		wind_buff.Play ();

	}
	void buff_energy() {
		rigidbody.AddRelativeForce (Vector3.forward * jump_power_duck * 5f);
		energy_buff.Stop ();
		energy_buff.Play ();
	}
	void shoot_lighting_buff () {
		lighting_buff_timer = Time.time + 0.065f;
		lighting_line.SetWidth (Random.Range (0.8f, 1f), 1f);
		lighting_line.gameObject.GetComponent<AudioController> ().playOnce (0);
		lighting_line.enabled = true;
		lighting_light.enabled = true;
		lighting_buff_shoot_particle.Stop ();
		lighting_buff_shoot_particle.Play ();
		lightingRay.origin = troll_spawnpoint.transform.position;
		lightingRay.direction = troll_spawnpoint.transform.forward;
		if (Physics.Raycast (lightingRay, out lightingHit, 200f)) {
			Instantiate (lightingHit_prefab, lightingHit.point, Quaternion.identity);
			lighting_line.SetPosition (1, lightingHit.point);
			if (lightingHit.collider.gameObject.tag == "enemy_small" || lightingHit.collider.gameObject.tag == "enemy_big") {
				lightingHit.collider.gameObject.GetComponent<EnemyHealth_manager> ().takeDamage (10);
				lightingHit.collider.gameObject.GetComponent<Rigidbody> ().AddForce (Vector3.up * 100f);	// add force to hit object
			} else if (lightingHit.collider.gameObject.tag == "enemy_boss") {
				lightingHit.collider.gameObject.GetComponentInParent<EnemyHealth_manager> ().takeDamage (15);
			}
		} else {
			lighting_line.SetPosition (1, lightingRay.origin + lightingRay.direction * 200f);
		}
		// play sound
		// raycast...
	}
	bool check_is_buffing() {
		return is_buff_fire || is_buff_lighting || is_buff_wind;
	}

}