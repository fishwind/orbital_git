﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	Animator anim;
	CanvasGroup canvasGroup;

	public bool IsOpen {
		get { return anim.GetBool("IsOpen");}
		set { anim.SetBool("IsOpen", value); }
	}

	public void Awake() {
		anim = GetComponent<Animator>();
		canvasGroup = GetComponent<CanvasGroup>();
		var rect = GetComponent<RectTransform>();
		rect.offsetMax = rect.offsetMin = new Vector2(0, 0);
	}
	public void Update() {
		if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("Open")) {
			canvasGroup.blocksRaycasts = canvasGroup.interactable = false;
		} else {
			canvasGroup.blocksRaycasts = canvasGroup.interactable = true;
		}
	}
}
