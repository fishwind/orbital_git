﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Boss_AI_lazer : MonoBehaviour {

	public Boss_AI boss_AI;
	public EnemyDamageLine dmgLine;
	public GameObject lazerStartingParticle;
	public float dmgLine_amount = 200f;
	public float dmgLine_force = 0f;
	public float dmgLine_throwUp = 0f;
	public bool dmgLine_isBig = true;
	public float shootDelay_high = 10f;
	public float shootDelay_mid = 8f;
	public float shootDelay_low = 6f;

	Animator anim;
	AudioController audioController;
	float shootDelay = 10f;
	float shootTimer = 0f;

	void Start() {
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		shootDelay = shootDelay_high;
		shootTimer = Time.time + shootDelay * 2;
	}

	void Update() {
		switch (boss_AI.getHealthLevel ()) {
			case "high":
				shootDelay = shootDelay_high;
				break;
			case "mid":
				shootDelay = shootDelay_mid;
				break;
			case "low":
				shootDelay = shootDelay_low;
				break;
		}
		if (!boss_AI.isDead && !boss_AI.isShootingMini && Time.time > shootTimer) {
			anim.SetTrigger ("A_shootLazer");
			shootTimer = Time.time + shootDelay;
			boss_AI.isShootingLazer = true;
			lazerStartingParticle.GetComponent<ParticleSystem> ().Stop ();
			lazerStartingParticle.GetComponent<ParticleSystem> ().Play ();
		}
	}

	void startLazer() {
		dmgLine.gameObject.GetComponent<LineRenderer>().enabled = true;
		dmgLine.startDmgLine (dmgLine_amount, dmgLine_force, dmgLine_throwUp, dmgLine_isBig, false);
		audioController.playOnce (0);
	}
	public void endLazer(){
		anim.SetTrigger ("A_idle");
		dmgLine.gameObject.GetComponent<LineRenderer>().enabled = false;
		lazerStartingParticle.GetComponent<ParticleSystem> ().Stop ();
		dmgLine.endDmgLine ();
		boss_AI.isShootingLazer = false;
	} 
}
