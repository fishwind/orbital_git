﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class Cactus : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageSphere dmgSphere;
	public float att_distance = 2f;
	public float chase_distance = 120f;
	public float move_speed = 5f;
	public float dmgSphere_amount = 200f;
	public float dmgSphere_force = 1500;
	public float dmgSphere_forceUp = 0.5f;
	public bool dmgSphere_isBig = true;
	public float minScale = 1f;
	public float maxScale = 1.5f;

	Animator anim;
	AudioController audioController;
	GameObject player;
	float att_timer = 0f;
	float att_cd = 1.5f;
	float turn_delay = 0.5f;
	float currentHealth;
	bool isDead = false;

	void Awake() {
		health.enemyType = 3;
	}
	void Start() {
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		currentHealth = health.currentHealth;
		transform.localScale = transform.localScale * Random.Range (minScale, maxScale);
	}
	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			} else {
				if (currentHealth != health.currentHealth) {
					currentHealth = health.currentHealth;
					hurt ();
				} else if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("Cactus_hurt")) {
					Vector3 vectorDiff = player.transform.position - this.transform.position;
					vectorDiff.y = 0;
					transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation(vectorDiff), turn_delay);
					float distance = Vector3.Distance (this.transform.position, player.transform.position);
					float angle = Vector3.Angle (this.transform.forward, vectorDiff);
					if (distance <= chase_distance && angle <= 70f) {
						if (distance <= att_distance) {
							if (Time.time > att_timer) {
								att ();
							}
						} else if(!anim.GetCurrentAnimatorStateInfo(0).IsName("Cactus_att")){
							chase ();
						}
					}
				}
			}
		}
	}

	void att () {
		att_timer = Time.time + att_cd;
		anim.SetBool("A_run", false);
		anim.SetTrigger ("A_att");
	}
	void startDmg() {
		dmgSphere.startDmgSphere (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
	}
	void endDmg() {
		dmgSphere.endDmgSphere ();
	}
	void chase() {
		anim.SetBool ("A_run", true);
		transform.Translate (transform.InverseTransformDirection(transform.forward) * move_speed * Time.deltaTime);
	}
	void hurt() {
		if (health.currentHealth >= 0) {
			anim.SetBool ("A_run", false);
			anim.SetTrigger ("A_hurt");
			audioController.playOnce (1);
		}
	}
	void die() {
		GameManager.stats_enemiesKilled += 1;
		anim.SetBool("A_run", false);
		anim.SetTrigger ("A_die");
		audioController.playOnce (0);
	}
	void clearBody() {
		GetComponent<BoxCollider> ().enabled = false;
		GetComponent<Rigidbody> ().isKinematic = false;
		Destroy (this.gameObject, 5f);
	}
}