﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Spider : MonoBehaviour {

	public EnemyHealth_manager health;
	public EnemyDamageSphere dmgSphere;
	public GameObject body;
	public Material[] material_list;
	public float jump_power_up_min = 250;
	public float jump_power_up_max = 500f;
	public float jump_power_forward_min = 400f;
	public float jump_power_forward_max = 600f;
	public float chase_distance = 120f;
	public float jump_cd_min = 0.3f;
	public float jump_cd_max = 1f;
	public float dmgSphere_amount = 200f;
	public float dmgSphere_force = 1500;
	public float dmgSphere_forceUp = 0.5f;
	public bool dmgSphere_isBig = true;
	public float minScale = 0.3f;
	public float maxScale = 3f;

	Animation anim;
	AudioController audioController;
	GameObject player;
	float jump_timer = 0f;
	float currentHealth;
	float turn_delay = 500f;
	bool isDead = false;

	void Awake() {
		health.enemyType = 3;
	}
	void Start() {
		anim = GetComponent<Animation> ();
		audioController = GetComponent<AudioController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		currentHealth = health.currentHealth;
		transform.localScale = transform.localScale * Random.Range (minScale, maxScale);
		body.GetComponent<SkinnedMeshRenderer>().material = material_list[Random.Range(0, material_list.Length)];
	}

	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				isDead = true;
				die ();
			} else {
				if (currentHealth != health.currentHealth) {
					currentHealth = health.currentHealth;
					hurt ();
				} else if (!anim.IsPlaying("spider_hurt") & anim.IsPlaying("spider_idle")) {
					Vector3 vectorDiff = player.transform.position - this.transform.position;
					vectorDiff.y = 0;
					transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (vectorDiff), turn_delay);
					float distance = Vector3.Distance (this.transform.position, player.transform.position);
					if (distance <= chase_distance) {
						if (Time.time > jump_timer) {
							jump ();
						}
					}
				}
			}
		}
	}

	void jump () {
		anim.CrossFade ("spider_jump");
	}
	void add_jumpForce() {
		GetComponent<Rigidbody> ().AddForce (Vector3.up * Random.Range(jump_power_up_min,jump_power_up_max));
		GetComponent<Rigidbody> ().AddForce (transform.forward * Random.Range(jump_power_forward_min, jump_power_forward_max));
		audioController.playOnce (2);
		endDmg ();
		startDmg ();
	}
	void startDmg() {
		dmgSphere.startDmgSphere (dmgSphere_amount, dmgSphere_force, dmgSphere_forceUp, dmgSphere_isBig, false);
	}
	void endDmg() {
		dmgSphere.endDmgSphere ();
	}
	void hurt() {
		endDmg ();
		anim.CrossFade ("spider_hurt");
		audioController.playOnce (1);
		jump_timer = Time.time + jump_cd_min;
	}
	void die() {
		GameManager.stats_enemiesKilled += 1;
		GetComponent<BoxCollider> ().enabled = false;
		GetComponent<Rigidbody> ().useGravity = false;
		endDmg ();
		anim.CrossFade ("spider_die");
		audioController.playOnce (0);
	}
	void clearBody() {
		GetComponent<Rigidbody> ().useGravity = true;
		Destroy (this.gameObject, 5f);
	}

	void OnCollisionEnter(Collision hit) {
		//if (hit.gameObject.tag == "ground") {
			endDmg ();
			anim.CrossFade ("spider_idle");
		jump_timer = Time.time + Random.Range (jump_cd_min, jump_cd_max);
		//}
	}
}
