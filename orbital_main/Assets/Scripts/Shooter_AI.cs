﻿using UnityEngine;
using System.Collections;

public class Shooter_AI : MonoBehaviour {

	public EnemyHealth_manager health;
	public GameObject spawnpoint;
	public GameObject bullet_prefab;
	public GameObject enemyExplosion;
	public GameObject shootEffect;
	public float bulletPower = 1500f;
	public float closeEnough_distance = 150f;

	GameObject player;
	Animator anim;
	AudioController audioController;
	bool isDead = false;
	float shootDelay = 5f;
	float shootTimer = 0f;

	void Awake() {
		health.enemyType = 2;
	}

	void Start() {
		player = GameObject.FindGameObjectWithTag ("Player_lookme");
		anim = GetComponent<Animator> ();
		audioController = GetComponent<AudioController> ();
	}

	void Update() {
		if (isDead == false) {
			if (health.currentHealth <= 0) {
				dead ();
				isDead = true;
			} else if (Time.time > shootTimer) {
				shootTimer = Time.time + shootDelay;
				shoot ();
			}
		}
	}
	void shoot() {
		float distance = Vector3.Distance (this.transform.position, player.transform.position);
		if (distance <= closeEnough_distance) {
			GameObject bul = (GameObject)(Instantiate (bullet_prefab, spawnpoint.transform.position, spawnpoint.transform.rotation));
			bul.GetComponent<Rigidbody> ().AddForce (spawnpoint.transform.forward * bulletPower);
			shootEffect.GetComponent<ParticleSystem> ().Stop ();
			shootEffect.GetComponent<ParticleSystem> ().Play ();
			audioController.playOnce (1);
		}
	}
	void dead() {
		GameManager.stats_enemiesKilled += 1;
		audioController.playOnce (0);
		Instantiate (enemyExplosion, transform.position, Quaternion.identity);
		GetComponent<BoxCollider> ().enabled = false;
		GetComponent<Rigidbody> ().isKinematic = false;
		Destroy (this.gameObject, 5f);
	}
}
